package IdeaBowlApplication.UserInterface

import IdeaBowlApplication.Core.Notes.Note
import IdeaBowlApplication.IdeaBowlInterface
import IdeaBowlApplication.RFIDInterface.TagId
import IdeaBowlApplication.UserInterface.Components.Controls.Controls
import IdeaBowlApplication.UserInterface.Components.Functionality.{AddNote, AssignTag, ShowNote, ShowNote2}
import com.phidget22.{RFIDTagEvent, RFIDTagLostEvent}
import scalafx.application.Platform
import scalafx.beans.property.{DoubleProperty, ReadOnlyDoubleProperty}
import scalafx.geometry.Pos.Center
import scalafx.geometry.{Orientation, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Alert.AlertType
import scalafx.scene.control.{Alert, Separator}
import scalafx.scene.input.MouseEvent
import scalafx.scene.layout.{FlowPane, HBox, VBox}
import scalafx.scene.text.Text

class HomeScreen extends Scene {

  /********/
  /* Pane */
  /********/
  /* The main pane of the home screen */
  private val pane = new HBox
  /* Main Screen Components */
  private val controlPane = new HBox
  private val contentPane = new FlowPane


  /* The variable indicating where wer are in the applications */
  var inHomeScreen = true
  var inShowNote = false
  var inAssignTag = false


  /* Titles */
  private val contentTitle = new Text("Idea")
  private val controlTitle = new Text("Controls")

  /* UI component variables  */
  var controlPaneWidth = DoubleProperty(0)
  var contentPaneWidth = DoubleProperty(0)

  /*****************/
  /* Read Tag Pane */
  /*****************/
  /* The pane showing a "read tag" message to query a note */
  private val readTagPane = new VBox
  private val readTagMessage = new Text("Read tag to get a note")


  /****************************/
  /* RFID Reader Interactions */
  /****************************/
  /* The method to call when a tag was detected */
  private def detectTag(tagEvent: RFIDTagEvent): Unit = {
    if (inHomeScreen || inShowNote) {
      Platform.runLater {
        /* Get the tag id from the detected tag */
        val tagId = TagId(tagEvent.getTag)
        try {
          /* Get a queried note for it, if possible  */
          val note = IdeaBowlInterface.bowlInstance.getNoteIdForTag(tagId)
          /* Show the note */
          showQueriedNote(note)
        }
        catch {
          case _: IllegalArgumentException =>
            /* Create an alert to notify the user that there is no category mapped to the given tag */
            val alert = new Alert(AlertType.Error)
            alert.title = "Unregistered Tag"
            alert.headerText = s"Tag ${tagId.id}"
            alert.contentText = "There is no mapping between the scanned tag and an existing category"
            /* Show the alert */
            alert.showAndWait()
          case _: NoSuchElementException =>
            /* Create an alert to notify the user that there was no noteId found for the given category (of the tag) */
            val alert = new Alert(AlertType.Error)
            alert.title = "No Note Found"
            alert.headerText = s"Tag ${tagId.id}"
            alert.contentText = "There was no note found for the category represented by the tag"
            /* Show the alert */
            alert.showAndWait()
        }
      }
    }
  }
  /* The method to call when a tag is lost */
  private def lostTag(tagLostEvent: RFIDTagLostEvent): Unit = {
    if (inHomeScreen) {
      Platform.runLater {
        /* Remove the show note and display the readTagPane */
        contentPane.children.clear()
        contentPane.children.addAll(readTagPane, new Separator)
      }
    }
  }
  IdeaBowlInterface.reader.addNewTagListener(detectTag)
  //IdeaBowlInterface.reader.addNewTagLostListener(lostTag)


  /************/
  /* Controls */
  /************/
  /* The method to show a queried note */
  def showQueriedNote(note: Note): Unit = {
    /* Clear Content pane */
    contentPane.children.clear()
    /* Add ShowNote UI module */
    val showNotePane = new ShowNote2
    showNotePane.initialise(note, contentPaneWidth, pane.height)
    contentPane.children.addAll(showNotePane, new Separator)
  }


  def loadShowNote(event: MouseEvent): Unit = {
    /* Clear Content pane */
    contentPane.children.clear()
    /* Add ShowNote UI module */
    val showNotePane = new ShowNote2
    /* Get a random note */
    val randomNote = IdeaBowlInterface.bowlInstance.getRandomNote
    showNotePane.initialise(randomNote, contentPaneWidth, pane.height)
    contentPane.children.addAll(showNotePane, new Separator)

    inHomeScreen = false
    inShowNote = true
    inAssignTag = false
  }

  /* Redirect to adding a new note */
  def loadAddNote(event: MouseEvent): Unit = {
    /* Clear Content pane */
    contentPane.children.clear()
    /* Add ShowNote UI module */
    val addNotePane = new AddNote
    addNotePane.initialise(contentPaneWidth, pane.height)
    contentPane.children.addAll(addNotePane, new Separator)

    inHomeScreen = false
    inShowNote = false
    inAssignTag = false
  }

  def loadAssignTag(event: MouseEvent): Unit = {
    /* Clear Content pane */
    contentPane.children.clear()
    /* Add AssignTag UI module */
    val assignTag = new AssignTag
    assignTag.initialise(contentPaneWidth, pane.height)
    contentPane.children.addAll(assignTag, new Separator)

    inHomeScreen = false
    inShowNote = false
    inAssignTag = true
  }


  /***********/
  /* Styling */
  /***********/

  // Stylesheet
  this.getStylesheets.add("IdeaBowlApplication/style.css")


  /******************/
  /* Initialisation */
  /******************/
  def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    /* Remove all children */
    pane.children.clear()
    content.clear()
    content = pane

    pane.prefWidth <== preferenceWidth
    pane.prefHeight <== preferenceHeight
    controlPaneWidth <== pane.prefWidth * 0.1
    contentPaneWidth <== pane.prefWidth * 0.9


    readTagPane.children.add(readTagMessage)
    contentPane.children.addAll(readTagPane, new Separator)
    readTagPane.prefWidth <== contentPaneWidth
    readTagPane.prefHeight <== height
    readTagPane.alignment = Pos.Center

    // list of all menu callbacks that will be passed to Controls class initialisation
    val menuCallbacks = List[MouseEvent => Unit](loadShowNote, loadAddNote, loadAssignTag)
    val menu = new Controls()
    var menuHeight = DoubleProperty(0)
    var menuWidth = DoubleProperty(0)
    menuHeight <== preferenceHeight
    menuWidth <== preferenceWidth / 15
    menu.initialise(menuWidth, menuHeight, menuCallbacks)

    controlPane.children.add(menu)

    /* UI Layout */
    pane.spacing = 1

    // ContentPane
    contentPane.prefWidth <== contentPaneWidth
    contentPane.prefHeight <== preferenceHeight

    // ControlPane
    controlPane.spacing = 100
    //controlPane.padding = Insets(100)
    controlPane.minWidth <== controlPaneWidth
    controlPane.prefWidth <== controlPaneWidth
    controlPane.prefHeight <== preferenceHeight
    controlPane.alignment = Center


    val separator = new Separator {
      orientation = Orientation.Vertical
      prefHeight <== preferenceHeight
    }
    pane.children.addAll(contentPane, separator, controlPane)
  }
}
