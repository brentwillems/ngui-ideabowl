package IdeaBowlApplication.UserInterface.Styling;

import com.sun.javafx.scene.control.skin.ButtonSkin;
import javafx.animation.FadeTransition;
import javafx.scene.control.Button;
import javafx.util.Duration;

public class SidebarButtonAnimation extends ButtonSkin {

    public SidebarButtonAnimation(Button control) {
        super(control);

        final FadeTransition fadeIn = new FadeTransition(Duration.millis(200));
        fadeIn.setNode(control);
        fadeIn.setToValue(1);
        control.setOnMouseEntered(e -> fadeIn.playFromStart());

        final FadeTransition fadeOut = new FadeTransition(Duration.millis(200));
        fadeOut.setNode(control);
        fadeOut.setToValue(0.8);
        control.setOnMouseExited(e -> fadeOut.playFromStart());

        control.setOpacity(0.8);

//        final Animation animation = new Transition() {
//            {
//                setCycleDuration(Duration.millis(20));
//                setCycleCount(10);
//            }
//
//            protected void interpolate(double frac) {
//                int count = this.getCycleCount();
//                double rVal = count * 10 / 255;
//                Color color = new Color(rVal, 0, 0, 0);
//
//                String hex = String.format("#%02x%02x%02x", ((int) color.getRed()), ((int) color.getGreen()), ((int) color.getBlue()));
//                String colorStyle = " -fx-background-color: " + hex + ";";
//                System.out.println(colorStyle);
//                control.setStyle(colorStyle);
//            }
//
//        };
//        control.setOnMouseEntered(e -> animation.playFromStart());

    }
}