package IdeaBowlApplication.UserInterface.Components.Controls

import java.beans.EventHandler

import IdeaBowlApplication.ProxyRFIDInterface.RFIDData
import IdeaBowlApplication.UserInterface
import IdeaBowlApplication.UserInterface.Elements.SidebarButton
import scalafx.beans.property.{DoubleProperty, ReadOnlyDoubleProperty}
import scalafx.event.ActionEvent
import scalafx.geometry.{Insets, Orientation, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.{Button, ScrollPane, Separator}
import scalafx.scene.layout.{FlowPane, HBox, TilePane, VBox}
import scalafx.scene.text.Text
import scalafx.Includes._
import scalafx.geometry.Pos.Center
import scalafx.scene.input.MouseEvent

class Controls extends VBox {

  val pane = new VBox()


  def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty, menuCallbacks: List[MouseEvent => Unit]): Unit ={
    prefWidth <== preferenceWidth
    prefHeight <== preferenceHeight
    pane.alignment = Center

    this.children = pane

    var buttonSize = DoubleProperty(0)
    buttonSize <== preferenceWidth * 0.75

    val assignTagButton = new SidebarButton
    assignTagButton.setText("Tags")
    assignTagButton.minHeight <== buttonSize
    assignTagButton.prefWidth <== buttonSize
    val addNoteButton = new SidebarButton
    addNoteButton.setText("Add")
    addNoteButton.minHeight <== buttonSize
    addNoteButton.prefWidth <== buttonSize
    val showNoteButton = new SidebarButton
    showNoteButton.setText("Display")
    showNoteButton.minHeight <== buttonSize
    showNoteButton.prefWidth <== buttonSize



    /* Button Event Handlers */
     showNoteButton.onMouseClicked = (event: MouseEvent) => menuCallbacks(0)(event)
     addNoteButton.onMouseClicked = (event: MouseEvent) => menuCallbacks(1)(event)
     assignTagButton.onMouseClicked = (event: MouseEvent) => menuCallbacks(2)(event)

    /* Lay out */
    //this.prefHeight <== preferenceHeight
    //this.prefWidth <== preferenceWidth

    this.spacing = 25
    //this.padding = Insets(10)

    this.children.addAll(showNoteButton, addNoteButton, assignTagButton)
  }
}
