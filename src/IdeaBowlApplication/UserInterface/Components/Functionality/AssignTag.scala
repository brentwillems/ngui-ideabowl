package IdeaBowlApplication.UserInterface.Components.Functionality

import java.util.NoSuchElementException

import IdeaBowlApplication.Core.Notes.Category
import IdeaBowlApplication.IdeaBowlInterface
import IdeaBowlApplication.RFIDInterface.TagId
import IdeaBowlApplication.UserInterface.Elements.MediaButton
import com.phidget22.{AttachEvent, DetachEvent, RFIDTagEvent, RFIDTagLostEvent}
import scalafx.application.Platform
import scalafx.beans.property.ReadOnlyDoubleProperty
import scalafx.collections.ObservableBuffer
import scalafx.geometry.{Insets, Orientation, Pos}
import scalafx.scene.control._
import scalafx.scene.layout.{HBox, VBox}
import scalafx.scene.paint.Color
import scalafx.scene.shape.Circle
import scalafx.scene.text.Text

class AssignTag extends VBox {

  /******************/
  /* Reader Display */
  /******************/
  /* The label to display when no reader is detected */
  private val noReaderText = "Reader not detected"
  private val detectedReaderDisplay = new Label(noReaderText) {contentDisplay = ContentDisplay.Left}

  /* The message to display when a reader is detected */
  private def readerDetected(readerId: String): Unit = {
    detectedReaderDisplay.graphic = createCircle(size = 10, color = Color.Green)
    detectedReaderDisplay.text = s"Reader $readerId detected"
  }
  /* The message to display when a reader is not detected */
  private def readerNotDetected(): Unit = {
    detectedReaderDisplay.graphic = createCircle(size = 10, color = Color.Red)
    detectedReaderDisplay.text = "Reader not detected"
  }

  /***************/
  /* Tag Display */
  /***************/
  /* The label to display when no tag is detected */
  private val noTagText = "No tag detected, please scan tag with the reader."
  /* The object displaying whether or not a tag has been detected */
  private val detectedTagDisplay: Label = new Label(noTagText) {contentDisplay = ContentDisplay.Top; wrapText = true}

  /* The method to create a new label for a detected tag */
  private def showNewTagLabel(tagId: TagId): Unit = {
    detectedTagDisplay.graphic = createCircle(size = 50, color = Color.Green)
    try {
      val category = IdeaBowlInterface.bowlInstance.getTagMapping(tagId)
      detectedTagDisplay.text = s"Tag ${tagId.id} -> ${category.name}"
    }
    catch {
      case _: NoSuchElementException =>
        detectedTagDisplay.text = s"Tag ${tagId.id}, has no mapping"
    }
  }
  /* Remove a tag from being displayed */
  private def removeTagLabel(): Unit = {
    detectedTagDisplay.graphic = createCircle(size = 50, color = Color.Red)
    detectedTagDisplay.text = noTagText
  }

  /* Lost a tag  */
  private def lostTagLabel(): Unit = {
    detectedTagDisplay.graphic = createCircle(size = 50, color = Color.DarkOrange)
    detectedTagDisplay.text = detectedTagDisplay.getText + " (Not detected)"
  }

  /* The method to create a circle of a given radius and colour */
  private def createCircle(size: Double, color: Color): Circle = new Circle {radius = size; fill = color}


  /****************/
  /* Detected Tag */
  /****************/
  /* The property indicating if a tag is detected */
  private val noTagID = "NO_TAG_ID"
  /* The tag id of the currently detected tag */
  private var currentTagId = noTagID


  /**********************/
  /* All Tag Categories */
  /**********************/
  private val tagState = new HBox(5)
  /* The pane for all the UI nodes of tag categories */
  private val tagCategoryPane = new VBox(5)
  /* The list view of all tag categories */
  private val allTagsView = new ListView[String]()
  private val allTags = new ObservableBuffer[String]
  allTagsView.items = allTags
  allTagsView.selectionModel.value.setSelectionMode(SelectionMode.Single)
  /* Add the current set of tags to all tags */
  IdeaBowlInterface.bowlInstance.setOfDefinedCategories.foreach(tag => allTags.add(tag.name))


  private val chooseCategory = new Text("Choose a tag category to link the detected tag to")
  private val categoryButtonsPane = new HBox(5)
  //noinspection ForwardReference
  private val mapButton = new MediaButton("Map tag") {
    onMouseClicked = _ => {
      /* Get the currently detected tag */
      if (currentTagId != noTagID) {
        /* Get the current selection */
        val selection = allTagsView.selectionModel.value.getSelectedItem
        /* Get the tag category */
        val tag = Category(selection)
        /* Map the tag to the detected RFID tag */
        IdeaBowlInterface.bowlInstance.insertTagMapping(TagId(currentTagId), tag)
        allmappings.removeIf(str => str.startsWith(currentTagId))
        allmappings.add(s"$currentTagId -> $selection")
      }
    }
  }
  /* The button to add a new tag */
  private val addTagButton = new MediaButton("Add new Tag") {
    onMouseClicked = _ => {
      val dialog = new TextInputDialog {headerText = "Add new Tag"; contentText = "Enter the name of the new tag category"}
      val optionalTagName = dialog.showAndWait()
      if (optionalTagName.isDefined) {
        val tag = Category(optionalTagName.get)
        IdeaBowlInterface.bowlInstance.addCategory(tag)
        allTags.clear()
        IdeaBowlInterface.bowlInstance.setOfDefinedCategories.foreach(tag => allTags.add(tag.name))
      }
    }
  }
  //noinspection ForwardReference
  /* The button to remove a tag */
  private val removeTagButton = new MediaButton("Remove Tag") {
    onMouseClicked = _ => {
      /* Get the current selection */
      val selection = allTagsView.selectionModel.value.getSelectedItem
      val tag = Category(selection)
      IdeaBowlInterface.bowlInstance.removeCategory(tag)
      allTags.clear()
      IdeaBowlInterface.bowlInstance.setOfDefinedCategories.foreach(tag => allTags.add(tag.name))
      allmappings.clear()
      IdeaBowlInterface.bowlInstance.tagMapping.foreach(keyValue => {
        if (!IdeaBowlInterface.bowlInstance.setOfDefinedCategories.contains(keyValue._2)) IdeaBowlInterface.bowlInstance.tagMapping.remove(keyValue._1)
      })
      IdeaBowlInterface.bowlInstance.tagMapping.foreach(keyValue => allmappings.add(s"${keyValue._1.id} -> ${keyValue._2.name}"))
    }
  }

  /****************/
  /* Tag Mappings */
  /****************/
  /* The pane for all the UI nodes of tag categories */
  private val tagMappingPane = new VBox(5)
  /* The list view of all tag categories */
  private val tagMappingView = new ListView[String]()
  private val allmappings = new ObservableBuffer[String]
  tagMappingView.items = allmappings
  tagMappingView.selectionModel.value.setSelectionMode(SelectionMode.Single)
  /* Add the current set of tags to all tags */
  IdeaBowlInterface.bowlInstance.tagMapping.foreach(keyValue => allmappings.add(s"${keyValue._1.id} -> ${keyValue._2.name}"))

  private val mappingInfo = new Text("Current Mappings")
  private val removeButton = new MediaButton("Remove mapping") {
    onMouseClicked = _ => {
      /* Get the current selection */
      val selection = tagMappingView.selectionModel.value.getSelectedItem
      if (selection != null) {
        /* Get the tag category */
        val tag = Category(selection)
        /* Remove the mapping */
        IdeaBowlInterface.bowlInstance.removeTagMapping(TagId(currentTagId))
        allmappings.removeIf(str => str.endsWith(tag.name))
      }
    }
  }

  /****************************/
  /* RFID Reader Interactions */
  /****************************/
  private def detectReader(attachEvent: AttachEvent): Unit = {
    Platform.runLater(readerDetected(attachEvent.getSource.getPhidgetIDString))
  }
  private def lostReader(detachEvent: DetachEvent): Unit = {
    Platform.runLater {
      readerNotDetected()
      removeTagLabel()
      currentTagId = noTagID
    }
  }
  private def detectTag(tagEvent: RFIDTagEvent): Unit = {
    if (IdeaBowlInterface.homeScreen.inAssignTag) {
      Platform.runLater {
        /* Get the tag id from the detected tag */
        val tagId = TagId(tagEvent.getTag)
        currentTagId = tagId.id
        /* Update the UI */
        showNewTagLabel(tagId)
      }
    }
  }
  private def lostTag(tagLostEvent: RFIDTagLostEvent): Unit = {
    if (IdeaBowlInterface.homeScreen.inAssignTag) {
      Platform.runLater {
        lostTagLabel()
        //currentTagId = noTagID
      }
    }
  }
  /* AssignTag should only be created once => assign to reader */
  /* Assign listeners to the reader and tag events */
  IdeaBowlInterface.reader.addNewAttachListener(detectReader)
  IdeaBowlInterface.reader.addNewDetachListener(lostReader)
  IdeaBowlInterface.reader.addNewTagListener(detectTag)
  IdeaBowlInterface.reader.addNewTagLostListener(lostTag)


  /******************/
  /* Initialisation */
  /******************/
  def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    prefWidth <== preferenceWidth
    prefHeight <== preferenceHeight * 0.99
    children.clear()
    children.addAll(detectedReaderDisplay, new Separator, detectedTagDisplay, new Separator, tagState)
    spacing = 10
    padding = Insets(5)
    alignment = Pos.TopCenter

    allTagsView.prefWidth = 300
    allTagsView.prefHeight <== prefHeight * 0.2
    tagMappingView.prefWidth <== allTagsView.prefWidth
    tagMappingView.prefHeight <== allTagsView.prefHeight

    categoryButtonsPane.children.addAll(addTagButton, removeTagButton, mapButton)
    tagCategoryPane.children.addAll(chooseCategory, allTagsView, categoryButtonsPane)
    tagMappingPane.children.addAll(mappingInfo, tagMappingView, removeButton)
    val tagSeparator = new Separator() {orientation = Orientation.Vertical}
    tagState.children.addAll(tagCategoryPane, tagSeparator, tagMappingPane)
    tagState.alignment = Pos.Center


    /* Initialise labels */
    removeTagLabel()
    if (IdeaBowlInterface.reader.getAttached) readerDetected(IdeaBowlInterface.reader.getPhidgetIDString)
    else readerNotDetected()
  }

}
