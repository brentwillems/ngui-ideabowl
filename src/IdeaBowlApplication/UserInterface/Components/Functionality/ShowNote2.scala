package IdeaBowlApplication.UserInterface.Components.Functionality

import java.net.URL

import IdeaBowlApplication.Core.Notes.Data._
import IdeaBowlApplication.Core.Notes.{Category, Note}
import IdeaBowlApplication.Core.Util.Storage
import IdeaBowlApplication.UserInterface.Elements._
import com.sun.xml.internal.ws.dump.LoggingDumpTube.Position
import scalafx.beans.property.{DoubleProperty, ReadOnlyDoubleProperty}
import scalafx.geometry.Pos.Center
import scalafx.geometry.{HPos, Insets, Pos, VPos}
import scalafx.scene.control.{Button, Separator}
import scalafx.scene.image.ImageView
import scalafx.scene.layout._
import scalafx.scene.media.{MediaPlayer, MediaView}
import scalafx.scene.text.Text
import scalafx.util.Duration

import scala.collection.mutable.ListBuffer
import scala.reflect.io.Path

class ShowNote2 extends VBox {

  // Main Panes
  val pane = new VBox(5)
  // Sub Panes
  val titlePane = new HBox(5)
  val titleCategoriesPane = new HBox(5)
  val descriptionPane = new HBox(5)
  val linkPane = new HBox (5)
  val mediaPane = new GridPane()
  val mediaMainFilesPane = new GridPane()
  val mediaOtherFilesPane = new GridPane()

  /* Gridpane constraints */
  val column1 = new ColumnConstraints()
  column1.setPercentWidth(33)
  column1.halignment = HPos.Center
  mediaPane.getColumnConstraints.addAll(column1, column1, column1)// each get 50% of width

  val row1 = new RowConstraints()
  row1.setPercentHeight(45)
  row1.valignment = VPos.Center

  val row2 = new RowConstraints()
  row2.setPercentHeight(10)
  row2.valignment = VPos.Center
  mediaOtherFilesPane.getRowConstraints.addAll(row1,row2, row1)


  /* The information of the note */
  var noteId = new TitleText
  var dateCreated = new Text
  var idea = new IdeaText
  val tagCategories: scala.collection.mutable.ListBuffer[TagCategoryButton] = ListBuffer.empty
  var tags: List[Category] = List()
  var hyperlinks: List[Hyperlink] = List()


  /******************/
  /* Initialisation */
  /******************/
  def initialise(note: Note,
                 preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    prefWidth <== preferenceWidth
    prefHeight <== preferenceHeight * 0.75
//    hbarPolicy = ScrollBarPolicy.Never
//    vbarPolicy = ScrollBarPolicy.AsNeeded

    noteId.text = "Note #"+ note.noteId.number.toString
    dateCreated.text = "Created on: " + note.dateCreated.toString
    idea.text = note.idea
    idea.wrappingWidth <== preferenceWidth * 0.9

    tags = note.categories
//    var tagString = ""
//    tags.foreach(t => tagString += ", " + t.name)
//    tagString = "Tags:\n\n" + tagString.tail.tail
    tags.foreach(t => {
      val btn = new TagCategoryButton
      btn.setText(t.name)
      tagCategories.+=(btn)
    })
    //tagText.setText(tagString)


    // data
    var largeButtonSize = DoubleProperty(0)
    largeButtonSize <== preferenceHeight * 0.3
    var smallButtonSize = DoubleProperty(0)
    smallButtonSize <== preferenceHeight * 0.125

    val imageButton = new MediaButtonLarge
    imageButton.setText("Pictures")
    imageButton.minHeight <== largeButtonSize
    imageButton.prefWidth <== largeButtonSize

    val videoButton = new MediaButtonLarge
    videoButton.setText("Video")
    videoButton.minHeight <== largeButtonSize
    videoButton.prefWidth <== largeButtonSize

    val audioButton = new MediaButtonSmall
    audioButton.setText("Music")
    audioButton.minHeight <== smallButtonSize
    audioButton.prefWidth <== smallButtonSize

    val otherButton = new MediaButtonSmall
    otherButton.setText("Other")
    otherButton.minHeight <== smallButtonSize
    otherButton.prefWidth <== smallButtonSize



    hyperlinks = NoteData.loadHyperlinks(note)

    val hyperlinkPane = new VBox(5)
    var showHyperlinks = false
    if (hyperlinks.nonEmpty) {
      showHyperlinks = true
      hyperlinks.foreach(h => hyperlinkPane.children.add(h))
    }

    /* The list of all data files */
    val dataFiles = note.data
    /* The data pane, showing one image/media type at once */
    val dataPane = new BorderPane
    dataPane.prefHeight <== preferenceHeight /3
    /* The current index of the data shown */
    var currentIndex = 0
    val maxIndex = dataFiles.length - 1

    /* The method to show tha data at a given path */
    def showData(index: Int): Unit = {
      /* Get the data at the given index */
      val dataPath = dataFiles(index)
      /* Get the path to where the data of the note is stored */
      val pathToNoteData = Storage.getDataDirectory(note)
      /* Load the data */
      val data = NoteData.loadData(pathToNoteData./(dataPath))
      /* Data is supported */
      if (data.isLeft) {
        val supportedData = data.left.get
        val fitW = (preferenceWidth * 0.5).toDouble//256
        val fitH = (preferenceHeight * 0.4).toDouble//256
        val mediaH = 50
        /* Data is image */
        if (supportedData.isLeft) {
          val image = supportedData.left.get
          val imageView = new ImageView
          imageView.image = image
          imageView.fitWidth = fitW
          imageView.fitHeight = fitH
          imageView.preserveRatio = true
          dataPane.center = imageView
        }
        else {
          val media = supportedData.right.get
          /* Data is an audio file */
          if (media.isLeft) {
            val audio = media.left.get
            val mediaPlayer = new MediaPlayer(audio)
            val mediaView = new MediaView(mediaPlayer)
            mediaView.fitWidth = fitW
            mediaView.fitHeight = mediaH
            mediaView.preserveRatio = true
            val name = Path(new URL(audio.source).getPath).name.replaceAll("%20", " ").replaceAll("%", " ")
            val audioMenu = new VBox(2)
            audioMenu.alignment = Pos.Center
            val controls = new HBox(2)
            controls.prefWidth = fitW
            controls.prefHeight = mediaH
            controls.alignment = Pos.Center
            val playButton = new MediaButton("Play")
            val pauseButton = new MediaButton("Pause")
            val restartButton = new MediaButton("Restart")
            playButton.onMouseClicked = _ => {mediaPlayer.play()}
            pauseButton.onMouseClicked = _ => {mediaPlayer.pause()}
            restartButton.onMouseClicked = _ => {
              mediaPlayer.pause()
              mediaPlayer.seek(Duration(0))
              mediaPlayer.play()
            }
            controls.children.addAll(restartButton, playButton, pauseButton)
            audioMenu.children.addAll(new MediaText(name), controls)
            dataPane.center = audioMenu
          }
          else {
            val video = media.right.get
            val mediaPlayer = new MediaPlayer(video)
            val mediaView = new MediaView(mediaPlayer)
            mediaView.fitWidth = fitW
            mediaView.fitHeight = fitH
            mediaView.preserveRatio = true
            val name = Path(new URL(video.source).getPath).name.replaceAll("%20", " ").replaceAll("%", " ")
            val videoMenu = new VBox(2)
            videoMenu.alignment = Pos.Center
            val controls = new HBox(2)
            controls.alignment = Pos.Center
            controls.prefWidth = fitW
            controls.prefHeight = mediaH
            val playButton = new MediaButton("Play")
            val pauseButton = new MediaButton("Pause")
            val restartButton = new MediaButton("Restart")
            playButton.onMouseClicked = _ => {mediaPlayer.play()}
            pauseButton.onMouseClicked = _ => {mediaPlayer.pause()}
            restartButton.onMouseClicked = _ => {
              mediaPlayer.pause()
              mediaPlayer.seek(Duration(0))
              mediaPlayer.play()
            }
            controls.children.addAll(restartButton, playButton, pauseButton)
            videoMenu.children.addAll(new MediaText(name), mediaView, controls)
            dataPane.center = videoMenu
          }
        }
      }
      /* Unsupported data */
      else {
        val unsupportedData = data.right.get
        dataPane.center = unsupportedData
      }
    }

    /* The buttons to go to the previous and next data */
    val showPreviousButton = new MediaButton("Previous") {
      onMouseClicked = _ => {
        /* Get the previous index and load the data for it */
        currentIndex = if (currentIndex == 0) maxIndex else currentIndex - 1
        showData(currentIndex)
      }
    }
    val showNextButton = new MediaButton("Next") {
      onMouseClicked = _ => {
        /* Get the previous index and load the data for it */
        currentIndex = if (currentIndex == maxIndex) 0 else currentIndex + 1
        showData(currentIndex)
      }
    }

    //content = pane
    pane.prefWidth <== prefWidth * 0.95
    pane.prefHeight <== prefHeight * 0.75
    pane.padding = Insets(20)
    pane.spacing = 10
    //pane.fillWidth = true
//    pane.children.clear()
//    pane.children.addAll(noteId, new Separator, dateCreated, new Separator,
//      tagText, new Separator, idea, new Separator)

    titleCategoriesPane.children.clear()
    titleCategoriesPane.spacing = 10
    tagCategories.foreach(el => titleCategoriesPane.children.add(el))
    titlePane.children.clear()
    titlePane.spacing = preferenceWidth.toDouble /5
    titlePane.children.addAll(noteId, titleCategoriesPane)


    descriptionPane.children.clear()
    descriptionPane.children.addAll(idea)
    linkPane.children.clear()
    //linkPane.children.addAll()

    GridPane.setConstraints(imageButton, 0, 0)
    GridPane.setConstraints(videoButton, 1, 0)
    GridPane.setConstraints(mediaOtherFilesPane, 2, 0)

    GridPane.setConstraints(audioButton, 0, 0)
    GridPane.setConstraints(otherButton, 0, 2)



//    mediaMainFilesPane.children.clear()
//    mediaMainFilesPane.children.addAll(imageButton, videoButton)
//
//    mediaMainFilesPane.setAlignment(Center)
//    mediaOtherFilesPane.prefWidth <== preferenceWidth * 0.2

//    mediaOtherFilesPane.children.clear()
//    mediaOtherFilesPane.children.addAll(audioButton, otherButton)
//    mediaPane.children.clear()



//    mediaPane.children.addAll(imageButton, videoButton, mediaOtherFilesPane)
    //mediaPane.alignment = Pos.TopLeft

    pane.children.clear()
    pane.children.addAll(titlePane, dateCreated, new Separator, descriptionPane, new Separator, linkPane, new Separator, mediaPane)

    pane.children.add(dataPane)
    if (dataFiles.isEmpty) dataPane.children.add(new Text("No data files to show for the given note"))
    else {
      dataPane.left = showPreviousButton
      dataPane.right = showNextButton
      showData(0)
    }
    if (showHyperlinks) pane.children.addAll(new Separator, hyperlinkPane)

    this.children.add(pane)
  }
}
