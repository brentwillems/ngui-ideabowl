package IdeaBowlApplication.UserInterface.Components.Functionality

import java.io.File

import IdeaBowlApplication.Core.Notes.Category
import IdeaBowlApplication.IdeaBowlInterface
import IdeaBowlApplication.UserInterface.Elements.CategoryButton
import IdeaBowlApplication.UserInterface.Elements.{MediaButton, MediaText}
import scalafx.beans.property.ReadOnlyDoubleProperty
import scalafx.collections.ObservableBuffer
import scalafx.geometry.Insets
import scalafx.scene.control._
import scalafx.scene.layout.{FlowPane, HBox, VBox}
import scalafx.stage.{FileChooser, Stage}


class AddNote extends VBox {

  def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    this.prefWidth <== preferenceWidth
    this.prefHeight <== preferenceHeight * 0.99
    this.children.clear()

    /* Text fields */
    val ideaTextBox = new HBox
    val ideaText = new TextArea()
    ideaText.setPrefRowCount(4)
    ideaText.setPromptText("Enter your idea here")
    ideaText.clear()
    ideaTextBox.children.addAll(new MediaText("Idea"), ideaText)
    //ideaTextBox.prefWidth <== prefWidth * 0.4
    //ideaTextBox.prefHeight <== prefHeight * 0.2

    /* Data files */
    /* The view for the list of data paths */
    val dataView = new ListView[String]
    dataView.prefWidth <== prefWidth * 0.4
    dataView.prefHeight <== prefHeight * 0.2
    /* The list of datafiles to display */
    val dataFiles = new ObservableBuffer[String]
    /* Set the list of files as the items of the view */
    dataView.items = dataFiles
    /* The file chooser, allowing the user to select the path to a data file */
    val fileChooser = new FileChooser()
    /* The button to add a new file to the list */
    val dataLoadButton = new MediaButton("Add File")
    dataLoadButton.onMouseClicked = event => {
      /* Open the url in the system's default browser (outside the application) */
      val file: File = fileChooser.showOpenDialog(new Stage())
      if (file != null) dataFiles.add(file.getAbsolutePath)
    }
    /* The button to delete a data file */
    val deleteDataButton = new MediaButton("Remove data from note")
    deleteDataButton.onMouseClicked = event => {
      /* Get the current selection from the data view and remove those items */
      val selections = dataView.selectionModel.value.getSelectedItems
      /* Remove the selected items from the list */
      selections.forEach(dataItem => dataFiles.remove(dataItem))
    }
    /* Some UI layout */
    val dataBox = new VBox(5)
    val dataTextBox = new HBox(5)
    val dataButtonsBox = new VBox(5)
    dataBox.children.addAll(new MediaText("Add data to note"), dataTextBox)
    dataButtonsBox.children.addAll(dataLoadButton, deleteDataButton)
    dataTextBox.children.addAll(dataView, dataButtonsBox)

    /* URLs */
    /* The view for the list of urls */
    val urlView = new ListView[String]
    urlView.prefWidth <== dataView.prefWidth
    urlView.prefHeight <== dataView.prefHeight
    /* The list of urls to display */
    val urls = new ObservableBuffer[String]
    /* Set the list of urls as the items of the view */
    urlView.items = urls
    /* The button to add a new url to the list */
    val addUrlButton = new MediaButton("Add URL")
    addUrlButton.onMouseClicked = event => {
      val dialog = new TextInputDialog
      dialog.headerText = "Add URL"
      dialog.contentText = "Enter the website link you wish to add to the note"
      val newUrl = dialog.showAndWait()
      if (newUrl.isDefined) urls.add(newUrl.get)
    }
    /* The button to delete a urls from the list */
    val deleteURLButton = new MediaButton("Remove URL from note")
    deleteURLButton.onMouseClicked = event => {
      /* Get the current selection from the url view and remove those items */
      val selections = urlView.selectionModel.value.getSelectedItems
      /* Remove the selected items from the list */
      selections.forEach(url => urls.remove(url))
    }
    /* Some UI layout */
    val urlBox = new VBox(5)
    val urlTextBox = new HBox(5)
    val urlButtonsBox = new VBox(5)
    urlBox.children.addAll(new MediaText("Add a website link"), urlTextBox)
    urlButtonsBox.children.addAll(addUrlButton, deleteURLButton)
    urlTextBox.children.addAll(urlView, urlButtonsBox)


    /* Categories */
    val categoriesBox = new FlowPane
    var categoryButtons = List[CategoryButton]()
    IdeaBowlInterface.bowlInstance.setOfDefinedCategories.foreach(
      category => {
        val button = new CategoryButton(category)
        categoryButtons ::= button
        categoriesBox.children.add(button)
      })

    /* Save Button */
    val saveButton = new MediaButton("Save")
    //saveButton.setText("Save")
    saveButton.onMouseClicked = event => {
      val tags = categoryButtons.filter(_.selected.value).map(_.category)

      /* Save note in bowl */
      IdeaBowlInterface.saveNote(ideaText.getText, dataFiles.toList, urls.toList, tags)
      /* Clear text fields */
      //inputFields.foreach(e => e.clear())
      ideaText.clear()
      dataFiles.clear()
      urls.clear()
      categoryButtons.foreach(button => button.selected = false)

      /* Give some visual feedback that note has been saved */
      //TODO!!
    }

    this.spacing = 10
    this.padding = Insets(5)

    ideaTextBox.spacing = 5
    dataTextBox.spacing = 5
    urlTextBox.spacing = 5
    categoriesBox.hgap = 5

    this.children.addAll(ideaTextBox, dataBox, urlBox, categoriesBox, new Separator, saveButton)

  }
}
