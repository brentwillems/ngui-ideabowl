package IdeaBowlApplication.UserInterface.Components.Functionality


import java.net.URL

import IdeaBowlApplication.Core.Notes.Data._
import IdeaBowlApplication.Core.Notes.{Category, Note}
import IdeaBowlApplication.Core.Util.Storage
import scalafx.beans.property.ReadOnlyDoubleProperty
import scalafx.geometry.{Insets, Pos}
import scalafx.scene.control.ScrollPane.ScrollBarPolicy
import scalafx.scene.control.{Button, ScrollPane, Separator}
import scalafx.scene.image.ImageView
import scalafx.scene.layout.{BorderPane, HBox, VBox}
import scalafx.scene.media.{MediaPlayer, MediaView}
import scalafx.scene.text.Text
import scalafx.util.Duration

import scala.reflect.io.Path

class ShowNote extends ScrollPane {

  val pane = new VBox(5)

  /* The information of the note */
  var noteId = new Text
  var dateCreated = new Text
  var idea = new Text
  var tags: List[Category] = List()
  var hyperlinks: List[Hyperlink] = List()


  /******************/
  /* Initialisation */
  /******************/
  def initialise(note: Note,
                 preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    prefWidth <== preferenceWidth
    prefHeight <== preferenceHeight * 0.75
    hbarPolicy = ScrollBarPolicy.Never
    vbarPolicy = ScrollBarPolicy.AsNeeded

    noteId.text = "Note ID: " + note.noteId.number.toString
    dateCreated.text = "Date Created: " + note.dateCreated.toString
    idea.text = "Note Idea:\n\n" + note.idea
    idea.wrappingWidth <== preferenceWidth * 0.9

    tags = note.categories
    var tagString = ""
    tags.foreach(t => tagString += ", " + t.name)
    tagString = "Tags:\n\n" + tagString.tail.tail

    hyperlinks = NoteData.loadHyperlinks(note)

    val hyperlinkPane = new VBox(5)
    var showHyperlinks = false
    if (hyperlinks.nonEmpty) {
      showHyperlinks = true
      hyperlinks.foreach(h => hyperlinkPane.children.add(h))
    }

    /* The list of all data files */
    val dataFiles = note.data
    /* The data pane, showing one image/media type at once */
    val dataPane = new BorderPane
    /* The current index of the data shown */
    var currentIndex = 0
    val maxIndex = dataFiles.length - 1

    /* The method to show tha data at a given path */
    def showData(index: Int): Unit = {
      /* Get the data at the given index */
      val dataPath = dataFiles(index)
      /* Get the path to where the data of the note is stored */
      val pathToNoteData = Storage.getDataDirectory(note)
      /* Load the data */
      val data = NoteData.loadData(pathToNoteData./(dataPath))
      /* Data is supported */
      if (data.isLeft) {
        val supportedData = data.left.get
        val fitW = 256
        val fitH = 256
        val mediaH = 50
        /* Data is image */
        if (supportedData.isLeft) {
          val image = supportedData.left.get
          val imageView = new ImageView
          imageView.image = image
          imageView.fitWidth = fitW
          imageView.fitHeight = fitH
          imageView.preserveRatio = true
          dataPane.center = imageView
        }
        else {
          val media = supportedData.right.get
          /* Data is an audio file */
          if (media.isLeft) {
            val audio = media.left.get
            val mediaPlayer = new MediaPlayer(audio)
            val mediaView = new MediaView(mediaPlayer)
            mediaView.fitWidth = fitW
            mediaView.fitHeight = mediaH
            mediaView.preserveRatio = true
            val name = Path(new URL(audio.source).getPath).name.replaceAll("%20", " ").replaceAll("%", " ")
            val audioMenu = new VBox(2)
            audioMenu.alignment = Pos.Center
            val controls = new HBox(2)
            controls.prefWidth = fitW
            controls.prefHeight = mediaH
            controls.alignment = Pos.Center
            val playButton = new Button("Play")
            val pauseButton = new Button("Pause")
            val restartButton = new Button("Restart")
            playButton.onMouseClicked = _ => {mediaPlayer.play()}
            pauseButton.onMouseClicked = _ => {mediaPlayer.pause()}
            restartButton.onMouseClicked = _ => {
              mediaPlayer.pause()
              mediaPlayer.seek(Duration(0))
              mediaPlayer.play()
            }
            controls.children.addAll(restartButton, playButton, pauseButton)
            audioMenu.children.addAll(new Text(name), controls)
            dataPane.center = audioMenu
          }
          else {
            val video = media.right.get
            val mediaPlayer = new MediaPlayer(video)
            val mediaView = new MediaView(mediaPlayer)
            mediaView.fitWidth = fitW * 2
            mediaView.fitHeight = fitH * 2
            mediaView.preserveRatio = true
            val name = Path(new URL(video.source).getPath).name.replaceAll("%20", " ").replaceAll("%", " ")
            val videoMenu = new VBox(2)
            videoMenu.alignment = Pos.Center
            val controls = new HBox(2)
            controls.alignment = Pos.Center
            controls.prefWidth = fitW
            controls.prefHeight = mediaH
            val playButton = new Button("Play")
            val pauseButton = new Button("Pause")
            val restartButton = new Button("Restart")
            playButton.onMouseClicked = _ => {mediaPlayer.play()}
            pauseButton.onMouseClicked = _ => {mediaPlayer.pause()}
            restartButton.onMouseClicked = _ => {
              mediaPlayer.pause()
              mediaPlayer.seek(Duration(0))
              mediaPlayer.play()
            }
            controls.children.addAll(restartButton, playButton, pauseButton)
            videoMenu.children.addAll(new Text(name), mediaView, controls)
            dataPane.center = videoMenu
          }
        }
      }
      /* Unsupported data */
      else {
        val unsupportedData = data.right.get
        dataPane.center = unsupportedData
      }
    }

    /* The buttons to go to the previous and next data */
    val showPreviousButton = new Button("Previous") {
      onMouseClicked = _ => {
        /* Get the previous index and load the data for it */
        currentIndex = if (currentIndex == 0) maxIndex else currentIndex - 1
        showData(currentIndex)
      }
    }
    val showNextButton = new Button("Next") {
      onMouseClicked = _ => {
        /* Get the previous index and load the data for it */
        currentIndex = if (currentIndex == maxIndex) 0 else currentIndex + 1
        showData(currentIndex)
      }
    }

    content = pane
    pane.prefWidth <== prefWidth * 0.95
    pane.prefHeight <== prefHeight * 0.75
    pane.children.clear()
    pane.children.addAll(noteId, new Separator, dateCreated, new Separator,
      new Text(tagString), new Separator, idea, new Separator)

    pane.padding = Insets(5)
    pane.spacing = 10
    //pane.fillWidth = true

    pane.children.add(dataPane)
    if (dataFiles.isEmpty) dataPane.children.add(new Text("No data files to show for the given note"))
    else {
      dataPane.left = showPreviousButton
      dataPane.right = showNextButton
      showData(0)
    }
    if (showHyperlinks) pane.children.addAll(new Separator, hyperlinkPane)

  }
}
