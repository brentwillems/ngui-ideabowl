package IdeaBowlApplication.UserInterface.Elements

import scalafx.scene.text.Text

class MediaText(text: String) extends Text(text){
    this.setId("MediaText")
}
