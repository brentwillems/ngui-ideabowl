package IdeaBowlApplication.UserInterface.Elements

import IdeaBowlApplication.Core.Notes.Category
import scalafx.scene.control.ToggleButton

class CategoryButton(val category: Category) extends ToggleButton {
  this.setId("CategoryButton")
  this.text = category.name
}
