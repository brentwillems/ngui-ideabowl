package IdeaBowlApplication.UserInterface.Elements

import scalafx.scene.control.Button

class MediaButton(name: String) extends Button(name) {
  this.setId("MediaButton")
}
