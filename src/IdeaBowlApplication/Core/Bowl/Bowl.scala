package IdeaBowlApplication.Core.Bowl

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import IdeaBowlApplication.Core.Bowl.Dictionaries.CategoryDictionary
import IdeaBowlApplication.Core.Bowl.Keys.DictionaryKey
import IdeaBowlApplication.Core.Notes.{Category, Note, NoteId}
import IdeaBowlApplication.Core.Util.Storage
import IdeaBowlApplication.RFIDInterface.TagId

import scala.collection.mutable
import scala.util.Random

/* The Bowl class, representing an idea bowl, providing access to its notes */
class Bowl extends Serializable {

  /*************************/
  /* Category Dictionaries */
  /*************************/
  /* The mapping of categories and their corresponding dictionaries */
  private val categoryDictionaries: mutable.Map[Category, CategoryDictionary] = mutable.Map()

  /* The getter and setter for a given category dictionary */
  private def getCategoryDictionary(category: Category): CategoryDictionary = {
    categoryDictionaries.getOrElse(category,
                                   throw new NoSuchElementException(s"There is no Category dictionary for the given category: $category"))
  }

  /****************/
  /* Defined Tags */
  /****************/
  /* The set of currently defined categories (no doubles allowed) */
  var setOfDefinedCategories: Set[Category] = Set()

  /* The method to add a category */
  def addCategory(newCategory: Category): Unit = {
    /* Add the category to the set of categories */
    setOfDefinedCategories += newCategory
    /* If there was no category dictionary present for the given category, create one */
    if (!categoryDictionaries.contains(newCategory)) categoryDictionaries += (newCategory -> new CategoryDictionary)
    /* Re-save the categories */
    Category.storeTags(setOfDefinedCategories)
    /* Re-save the bowl */
    Bowl.saveBowl(this)
  }
  /* The method to remove a category */
  def removeCategory(category: Category): Unit = {
    /* Remove the category from the set of categories */
    setOfDefinedCategories -= category
    /* If there was a category dictionary present for the given category, remove it */
    if (!categoryDictionaries.contains(category)) categoryDictionaries -= category
    /* Re-save the categories */
    Category.storeTags(setOfDefinedCategories)
    /* Re-save the bowl */
    Bowl.saveBowl(this)
  }

  /***************/
  /* Tag Mapping */
  /***************/
  /* The mapping between tag ids and categories */
  var tagMapping: mutable.Map[TagId, Category] = mutable.Map()

  /* Method to return a category for a given tag id if it has a mapping */
  def getTagMapping(tagId: TagId): Category = {
    tagMapping.getOrElse(tagId, throw new NoSuchElementException(s"No category stored for the given tagId $tagId"))
  }

  /* The method to insert a new category for a given tag id */
  def insertTagMapping(tagId: TagId, category: Category): Unit = {
    /* Add the mapping */
    tagMapping += (tagId -> category)
    /* Re-save the bowl */
    Bowl.saveBowl(this)
  }
  /* The method to remove a category for a given tag id */
  def removeTagMapping(tagId: TagId): Unit = {
    /* Remove the mapping */
    tagMapping -= tagId
    /* Re-save the bowl */
    Bowl.saveBowl(this)
  }

  /******************/
  /* Managing Notes */
  /******************/
  /* The method to get a note with a given note id */
  def getNote(noteId: NoteId): Note = Note.loadNote(noteId)

  /* The method to insert a new note into the bowl */
  def insertNote(note: Note): Unit = {
    /* Add the note to the appropriate dictionaries */
    note.categories.foreach(category => {
      /* Get the dictionary for the given category */
      val dictionary = getCategoryDictionary(category)
      /* Generate a dictionary key for the note */
      val key = DictionaryKey.generateKey(note)
      /* Add the noteId of the given note to the dictionary */
      dictionary.insertNoteId(key, note.noteId)
    })
    /* Save the note to the filesystem */
    Note.saveNote(note)
    /* Re-save the bowl */
    Bowl.saveBowl(this)
  }

  /* The method to delete a note from the bowl */
  def removeNote(note: Note): Unit = {
    /* Remove the note from the appropriate dictionaries */
    note.categories.foreach(category => {
      /* Get the dictionary for the given category */
      val dictionary = getCategoryDictionary(category)
      /* Generate a dictionary key for the note */
      val key = DictionaryKey.generateKey(note)
      /* Add the noteId of the given note to the dictionary */
      dictionary.removeNoteForKey(key)
    })
    /* Remove the note from the filesystem */
    Note.removeNote(note)
    /* Re-save the bowl */
    Bowl.saveBowl(this)
  }

  /****************/
  /* Note Queries */
  /****************/
  /* The method to get a note from the given category, oldest one first or random */
  def getNoteIdForTag(tagId: TagId, returnOldest: Boolean = true): Note = {
    /* Check if the given tagId has a mapping for a category */
    if (!tagMapping.contains(tagId)) {
      throw new IllegalArgumentException(s"No category is mapper for the given tagId ${tagId.id}")
    }
    /* Get the category mapped to the given tag id */
    val category = tagMapping(tagId)
    /* Check if the given category has a dictionary */
    if (!categoryDictionaries.contains(category)) {
      throw new NoSuchElementException(s"No dictionary found for the given category $category")
    }
    /* Get the dictionary for the given category */
    val categoryDictionary = getCategoryDictionary(category)
    /* Check if the dictionary has at least one noteId */
    if (categoryDictionary.isEmpty) {
      throw new NoSuchElementException(s"No noteId stored in the dictionary for the given category $category")
    }
    /* Return the oldest value */
    if (returnOldest) {
      /* Load the note from the filesystem */
      Note.loadNote(categoryDictionary.getFirst._2)
    }
    else {
      /* Get all the values */
      val allValues = categoryDictionary.getNoteIdIterator.toArray
      /* Get a random index */
      val randomIndex = Random.nextInt(allValues.length)
      /* Load the note from the filesystem */
      Note.loadNote(allValues(randomIndex))
    }
  }

  /* The method to get a random note from the bowl */
  def getRandomNote: Note = {
    /* Get all the noteIds in all dictionaries */
    val allEntries = categoryDictionaries.foldLeft(List[NoteId]())((result, current) => result ++ current._2.getNoteIdIterator.toList)
    /* If there are not entries, then there are not notes stored */
    if (allEntries.isEmpty) throw new NoSuchElementException(s"There are no notes stored in the bowl")
    /* Get a random index */
    val randomIndex = Random.nextInt(allEntries.length)
    /* Get the noteId at that index and return the corresponding note */
    Note.loadNote(allEntries(randomIndex))
  }

}


object Bowl {

  /****************/
  /* Bowl Storage */
  /****************/
  /* The method to load a bowl from its file */
  def loadBowl(): Bowl = {
    /* Get the file to where the bowl should be stored */
    val bowlFile = Storage.bowlFile.toFile.jfile
    /* Create an object input stream to read the object from */
    val fileStream = new FileInputStream(bowlFile)
    val fileObjectStream = new ObjectInputStream(fileStream)
    /* Read the file in as a bowl instance */
    val bowl = fileObjectStream.readObject().asInstanceOf[Bowl]
    /* Close the input streams */
    fileObjectStream.close()
    fileStream.close()
    /* Return the bowl */
    bowl
  }

  def saveBowl(bowl: Bowl): Unit = {
    /* Get the directory and file to where the bowl should be stored */
    val bowlFile = Storage.bowlFile.toFile
    /* Create the file if necessary */
    if (!bowlFile.exists) bowlFile.createFile()
    /* Create an object output stream to write the object to */
    val fileStream = new FileOutputStream(bowlFile.jfile)
    val fileObjectStream = new ObjectOutputStream(fileStream)
    /* Write the bowl to the file */
    fileObjectStream.writeObject(bowl)
    /* Close the output streams */
    fileObjectStream.close()
    fileStream.close()
  }

  // TODO: remove main after setup
  def main(args: Array[String]): Unit = {
    /* Create a new bowl */
    val bowl = new Bowl
    /* The set of defined categories */
    val categories = Set(Category("Work"), Category("Health"), Category("Cat"), Category("Easy"), Category("School"),
                         Category("Presents"), Category("Urgent"), Category("Sport"), Category("Christmas"), Category("Food Recipes"))
    categories.foreach(category => bowl.addCategory(category))
    /* The tag mappings */
    val newMappings = List(TagId("4a003796c5") -> Category("Cat"),
                           TagId("3000d77fee") -> Category("Sport"),
                           TagId("3000d72f62") -> Category("School"),
                           TagId("5200544557") -> Category("Food Recipes"),
                           TagId("5200541fd9") -> Category("Christmas"),
                           TagId("5200541471") -> Category("Easy"))
    newMappings.foreach(entry => bowl.insertTagMapping(entry._1, entry._2))
    Range(0, 7).foreach(integer => {
      val note = Note.loadNote(NoteId(integer))
      bowl.insertNote(note)
    })
    /* Store the bowl to the filesystem */
    Bowl.saveBowl(bowl)
  }

}
