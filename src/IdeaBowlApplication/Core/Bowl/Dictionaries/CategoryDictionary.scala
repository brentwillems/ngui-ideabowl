package IdeaBowlApplication.Core.Bowl.Dictionaries

import IdeaBowlApplication.Core.Bowl.KeyOrderings.DictionaryKeyOrdering
import IdeaBowlApplication.Core.Bowl.Keys.DictionaryKey

/* The class representing dictionaries of noteIds for a given category */
class CategoryDictionary extends Dictionary[DictionaryKey](DictionaryKeyOrdering)
