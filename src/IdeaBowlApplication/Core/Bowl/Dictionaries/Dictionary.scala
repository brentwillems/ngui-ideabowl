package IdeaBowlApplication.Core.Bowl.Dictionaries

import java.util.NoSuchElementException

import IdeaBowlApplication.Core.Bowl.Keys.DictionaryKey
import IdeaBowlApplication.Core.Notes.NoteId

import scala.collection.mutable

/* The abstract class representing the default functionality that all dictionaries should implement */
abstract class Dictionary[K <: DictionaryKey](ordering: Ordering[K]) extends Serializable {

  /**************/
  /* Dictionary */
  /**************/
  /* The internal "dictionary" used to store the tuples of keys and noteIds */
  private val dictionary: mutable.TreeMap[K, NoteId] = new mutable.TreeMap[K, NoteId]()(ordering)


  /*********************************/
  /* Insert, Remove, Get, Contains */
  /*********************************/
  /* Method to insert a noteId into the dictionary */
  final def insertNoteId(key: K, noteId: NoteId): Unit = synchronized {
    /* If the dictionary doesn't contain the key/note pair yet, add it */
    if (!containsKeyNoteIdPair(key, noteId)) dictionary.put(key, noteId)
    /* Throw an exception as the key/note pair is already present in the dictionary */
    else throw new IllegalArgumentException(s"The provided key/noteId pair ($key, $noteId) is already present in the dictionary")
  }

  /* Method to remove a noteId from the dictionary, given a key */
  final def removeNoteForKey(key: K): Unit = synchronized { dictionary.remove(key) }

  /* Method to get a noteId from the dictionary, given a key */
  final def getNoteId(key: K): NoteId = synchronized {
    /* Try getting the noteId or throw an exception if there was no noteId found for the given key */
    dictionary.getOrElse(key, throw new NoSuchElementException(s"No noteId with the given key $key in the dictionary"))
  }

  /* Method to check if the dictionary contains a noteId for the given key */
  final def containsNoteIdForKey(key: K): Boolean = synchronized { dictionary.contains(key) }

  /* Method to check if a dictionary contains a given key/noteId pair */
  final def containsKeyNoteIdPair(key: K, noteId: NoteId): Boolean = synchronized {
    /* Check if the noteId is present in the dictionary */
    try {
      val idFound = getNoteId(key)
      /* Check if the noteId found is equal to the given noteId */
      idFound.equals(noteId)
    }
    /* The given key/noteId pair doesn't exist in the dictionary */
    catch {
      case _: NoSuchElementException => false
    }
  }

  /* Method to check if a noteId is present in the dictionary */
  final def containsNoteId(noteId: NoteId): Boolean = synchronized {
    /* Check if the noteId is present in the dictionary */
    dictionary.exists(((_: K, i: NoteId) => i.equals(noteId)).tupled)
  }

  /* Method to get the first key/noteId pair in the dictionary */
  final def getFirst: (K, NoteId) = synchronized { dictionary.head }
  /* Method to get the last key/noteId pair in the dictionary */
  final def getLast: (K, NoteId) = synchronized { dictionary.last }


  /***************************/
  /* Size, isEmpty, nonEmpty */
  /***************************/
  /* Method returning the amount of key/noteId pairs in the dictionary */
  final def size: Int = synchronized { dictionary.size }
  /* Method returning if the dictionary is empty */
  final def isEmpty: Boolean = synchronized { dictionary.isEmpty }
  /* Method returning if the dictionary is not empty */
  final def nonEmpty: Boolean = synchronized { dictionary.nonEmpty }


  /*************************************************/
  /* For each, Filter, Find, For all, Count, Range */
  /*************************************************/
  /* Method applying a foreach to the noteIds of the dictionary */
  final def foreach(function: (K, NoteId) => Unit): Unit = synchronized { dictionary.foreach(function.tupled) }

  /* Method applying a filter function to the key/noteId pairs in the dictionary */
  final def filter(predicateFunction: (K, NoteId) => Boolean): Array[(K, NoteId)] = synchronized {
    dictionary.filter(predicateFunction.tupled).toArray
  }

  /* Method finding the first key/noteId pair satisfying a predicate */
  final def find(predicateFunction: (K, NoteId) => Boolean): (K, NoteId) = synchronized {
    /* Try finding such a noteId */
    val optionalElement = dictionary.find(predicateFunction.tupled)
    optionalElement.getOrElse(throw new NoSuchElementException(s"No noteId in the dictionary satisfying the predicate"))
  }

  /* Method checking if a predicate holds for all key/noteId pairs in the dictionary */
  final def forAll(predicateFunction: (K, NoteId) => Boolean): Boolean = synchronized { dictionary.forall(predicateFunction.tupled) }
  /* Method counting the amount of key/noteId pairs that satisfy a predicate */
  final def count(predicateFunction: (K, NoteId) => Boolean): Int = synchronized { dictionary.count(predicateFunction.tupled) }

  /* Method to get all noteIds for keys between the given range */
  final def getRange(from: K, until: K): Iterator[(K, NoteId)] = dictionary.range(from, until).toIterator


  /*****************************************/
  /* Iterate, Iterate keys, Iterate Values */
  /*****************************************/
  /* Method returning a key/noteId iterator for the dictionary */
  final def getIterator: Iterator[(K, NoteId)] = synchronized { dictionary.iterator }

  /* Method returning a key iterator for the dictionary */
  final def getKeyIterator: Iterator[K] = synchronized { dictionary.keysIterator }

  /* Method returning a noteId iterator for the dictionary */
  final def getNoteIdIterator: Iterator[NoteId] = synchronized { dictionary.valuesIterator }


  /*********************/
  /* Group by, Grouped */
  /*********************/
  /* Method to group item with the given function */
  final def groupBy[S](function: (K, NoteId) => S): Map[S, mutable.TreeMap[K, NoteId]] = synchronized {
    dictionary.groupBy(function.tupled)
  }

  /* Method to split the dictionary up in fixed size iterable collections */
  final def grouped(size: Int): Iterator[Iterator[(K, NoteId)]] = synchronized {
    dictionary.grouped(size).map(treeMap => treeMap.iterator)
  }

}

