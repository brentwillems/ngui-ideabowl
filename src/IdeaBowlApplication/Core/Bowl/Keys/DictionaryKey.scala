package IdeaBowlApplication.Core.Bowl.Keys

import IdeaBowlApplication.Core.Notes.{Date, Note, NoteId}

/* The case class representing a dictionary key */
case class DictionaryKey(date: Date)


object DictionaryKey {

  /****************/
  /* Generate Key */
  /****************/
  /* The method to generate a dictionary key for a given note */
  def generateKey(note: Note): DictionaryKey = DictionaryKey(note.dateCreated)

}
