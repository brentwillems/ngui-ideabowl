package IdeaBowlApplication.Core.Bowl.KeyOrderings

import IdeaBowlApplication.Core.Bowl.Keys.DictionaryKey

/* The object implementing an ordering for All Items Dictionary keys */
object DictionaryKeyOrdering extends Ordering[DictionaryKey] {

  /***********/
  /* Compare */
  /***********/
  override def compare(x: DictionaryKey, y: DictionaryKey): Int = {
    /* Compare by the note ID */
    x.date.compareTo(y.date)
  }

}
