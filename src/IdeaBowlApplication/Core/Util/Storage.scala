package IdeaBowlApplication.Core.Util

import IdeaBowlApplication.Core.Notes.{Note, NoteId}

import scala.reflect.io.Path

/* The object providing the paths to the different data stored by the application */
object Storage {

  /********************/
  /* Application Data */
  /********************/
  /* The path to the application data directory */
  private val applicationDataDirectory: Path = Path("AppData").toAbsolute


  /****************/
  /* Tags Storage */
  /****************/
  /* The path to the file containing all defined categories for notes */
  val categoriesFile: Path = applicationDataDirectory./("Categories")


  /****************/
  /* Bowl Storage */
  /****************/
  /* The path to the file containing the bowl of notes */
  val bowlFile: Path = applicationDataDirectory./("Bowl")


  /****************/
  /* Note Storage */
  /****************/
  /* The path to the note id file */
  val noteIdFile: Path = applicationDataDirectory./("NoteId")

  /* The path to the notes directory */
  val notesDirectory: Path = applicationDataDirectory./("Notes")
  /* Method to get the path to a given note directory */
  def getNoteDirectory(note: Note): Path = getNoteDirectory(note.noteId)
  def getNoteDirectory(noteId: NoteId): Path = notesDirectory./(noteId.number.toString)

  /* The name of a note file */
  private val noteFileName: String = "Note"
  /* Method to get the path to a given note file */
  def getNoteFile(note: Note): Path = getNoteDirectory(note)./(noteFileName)
  def getNoteFile(noteId: NoteId): Path = getNoteDirectory(noteId)./(noteFileName)


  /****************/
  /* Data Storage */
  /****************/
  /* The path to the data directory */
  private val dataDirectoryName: String = "Data"
  /* Method to get the path to a given note data directory */
  def getDataDirectory(note: Note): Path = getNoteDirectory(note)./(dataDirectoryName)

}
