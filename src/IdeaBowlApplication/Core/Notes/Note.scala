package IdeaBowlApplication.Core.Notes


import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import IdeaBowlApplication.Core.Util.Storage

/* The Note class, representing an idea note */
class Note extends Serializable {

  /***********/
  /* Note ID */
  /***********/
  /* The id of the note, used for retrieving its data */
  val noteId: NoteId = Note.getNextAvailableId


  /****************/
  /* Date Created */
  /****************/
  /* The date when the note was created */
  val dateCreated: Date = new Date


  /*************/
  /* Note Idea */
  /*************/
  /* The idea written on the note */
  var idea: String = _


  /*******************/
  /* Additional Data */
  /*******************/
  /* The paths to the additional data stored with the idea */
  var data: List[String] = List()
  /* The list of urls to display */
  var urls: List[URL] = List()


  /**************/
  /* Categories */
  /**************/
  /* The list of categories attached to the idea */
  var categories: List[Category] = List()

}


/* The companion object for notes */
object Note {

  /***********/
  /* Note Id */
  /***********/
  /* The next available id for a note */
  private var nextAvailableId: Int = 0

  /* The method to retrieve the next available id */
  def getNextAvailableId: NoteId = {
    /* Store the current id in a temporal variable */
    val id = nextAvailableId
    /* Update the id for the next time */
    nextAvailableId += 1
    storeNextId()
    /* Return the available id */
    NoteId(id)
  }

  /*******************/
  /* Note Id Storage */
  /*******************/
  /* The method to update the next available id from its file */
  def loadNextId(): Unit = {
    /* Get the file where the id is written */
    val idFile = Storage.noteIdFile.toFile
    /* Extract the id from the file */
    val id = idFile.slurp().toInt
    /* Update the nextAvailableId variable */
    nextAvailableId = id
  }

  /* The method to write the next available id to its file */
  def storeNextId(): Unit = {
    /* Get the file where the id should be written */
    val idFile = Storage.noteIdFile.toFile
    /* Write the current value to the file */
    idFile.writeAll(nextAvailableId.toString)
  }

  /****************/
  /* Note Storage */
  /****************/
  /* The method to load a note with a given id from its file */
  def loadNote(noteId: NoteId): Note = {
    /* Get the file to where the note should be stored */
    val noteFile = Storage.getNoteFile(noteId).toFile.jfile
    /* Create an object input stream to read the object from */
    val fileStream = new FileInputStream(noteFile)
    val fileObjectStream = new ObjectInputStream(fileStream)
    /* Read the file in as a note instance */
    val note = fileObjectStream.readObject().asInstanceOf[Note]
    /* Close the input streams */
    fileObjectStream.close()
    fileStream.close()
    /* Return the note */
    note
  }

  /* The method to save a note with a given id to its file */
  def saveNote(note: Note): Unit = {
    /* Get the directory and file to where the note should be stored */
    val noteDirectory = Storage.getNoteDirectory(note).toDirectory
    val noteFile = Storage.getNoteFile(note).toFile
    /* Create the directory and file if necessary */
    if (!noteDirectory.exists) noteDirectory.createDirectory()
    if (!noteFile.exists) noteFile.createFile()
    /* Create an object output stream to write the object to */
    val fileStream = new FileOutputStream(noteFile.jfile)
    val fileObjectStream = new ObjectOutputStream(fileStream)
    /* Write the note to the file */
    fileObjectStream.writeObject(note)
    /* Close the output streams */
    fileObjectStream.close()
    fileStream.close()
  }

  /* The method to remove a note with a given id from the filesystem */
  def removeNote(note: Note): Unit = {
    /* Get the directory to where the note should be stored */
    val noteDirectory = Storage.getNoteDirectory(note).toDirectory
    /* Delete the directory and all its files */
    noteDirectory.deleteRecursively()
  }


  // TODO: remove after database is set up & notes can be added by users to the application
  def main(args: Array[String]): Unit = {
    /**********/
    /* Note 0 */
    /**********/
    val note0 = new Note
    note0.idea = "Spend more time playing with my cat. Give her lots of cuddles and kisses, because she can get bored. Make sure she doesn't eat Herbie, the goldfish."
    note0.categories = List(Category("Cat"), Category("Health"), Category("Presents"), Category("Christmas"))
    note0.data = List("2014-09-02 16.19.52.png", "2014-09-13 14.02.48.mp4", "2016-10-19 19.17.43.jpeg")
    saveNote(note0)

    /**********/
    /* Note 1 */
    /**********/
    val note1 = new Note
    note1.idea = "Try the new tomato soup, it's super healthy and easy to prepare! Just like grandma used to make..."
    note1.categories = List(Category("Easy"), Category("Health"))
    note1.data = List("soup.jpg")
    note1.urls = List(URL("https://www.jamieoliver.com/recipes/vegetables-recipes/tomato-soup/"),
                      URL("https://www.inspiredtaste.net/27956/easy-tomato-soup-recipe/"))
    saveNote(note1)

    /**********/
    /* Note 2 */
    /**********/
    val note2 = new Note
    note2.idea = "Exercise at least twice a week for an hour. Maybe do some cardio."
    note2.categories = List(Category("Sport"), Category("Health"))
    note2.urls = List(URL("https://www.myactivesg.com/Read/2016/12/11-Best-Cardio-Workouts-to-burn-fats"))
    saveNote(note2)

    /**********/
    /* Note 3 */
    /**********/
    val note3 = new Note
    note3.idea = "Now that winter is coming, perhaps a new scarf and bonnet would be a nice present."
    note3.categories = List(Category("Cat"), Category("Presents"), Category("Christmas"))
    note3.data = List("2014-12-14 18.00.01.bmp")
    note3.urls = List(URL("https://www.petcha.com/japanese-style-dispatch-get-your-cat-a-scarf-trending/"),
                      URL("https://www.petmd.com/cat/training/evr_ct_how_to_walk_you_walk_your_cat"))
    saveNote(note3)

    /**********/
    /* Note 4 */
    /**********/
    val note4 = new Note
    note4.idea = "Finish the IdeaBowl application.\nTake another look at the requirements.\nGet the application running on a Raspberry PI."
    note4.categories = List(Category("Work"), Category("School"), Category("Urgent"))
    note4.data = List("Idea_Bowl_with_notes.jpg", "Group5_Requirements_Analysis.pdf", "NGUIProject2018-2019.pdf")
    note4.urls = List(URL("https://www.raspberrypi.org"))
    saveNote(note4)

    /**********/
    /* Note 5 */
    /**********/
    val note5 = new Note
    note5.idea = "Choose the background music for our application. Perhaps one of these, or other Youtube royalty-free music."
    note5.categories = List(Category("Work"), Category("School"))
    note5.data = List("Morning_Stroll - Youtube Free Music.mp3", "Atlanta - Youtube Free Music.mp3")
    note5.urls = List(URL("https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ"))
    saveNote(note5)

    /**********/
    /* Note 6 */
    /**********/
    val note6 = new Note
    note6.idea = "My cat likes to eat catnip. Maybe I should take her on a walk like we used to"
    note6.categories = List(Category("Cat"))
    note6.data = List("2015-06-20 11.02.05.jpg", "2016-01-14 19.27.00.flv")
    saveNote(note6)

  }

}
