package IdeaBowlApplication.Core.Notes

/* The case class representing a note id */
case class NoteId(number: Int)
