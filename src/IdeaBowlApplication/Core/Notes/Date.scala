package IdeaBowlApplication.Core.Notes

import java.text.DateFormat

/* The date class, used when creating new notes and to compare notes */
class Date extends java.util.Date with Serializable {

  /* Change the to string method to print the date nicer */
  override def toString: String = {
    DateFormat.getDateTimeInstance().format(this)
  }

}
