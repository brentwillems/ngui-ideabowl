package IdeaBowlApplication.Core.Notes

import IdeaBowlApplication.Core.Util.Storage

/* The class representing a tag category, used as a priority or filter for notes */
case class Category(name: String)

/* A tag is nothing more than a name (String) that is attached to notes,
 * therefore it provides no extra functionality */


/* The companion object for the note tags */
object Category {

  /*****************************/
  /* Load & Store Defined Tags */
  /*****************************/
  /* The method to update the defined tags from their file */
  def loadDefinedTags(): Set[Category] = {
    /* Get the file where the tags are written */
    val tagsFile = Storage.categoriesFile.toFile
    val reader = tagsFile.bufferedReader()
    /* Extract the tags from the file */
    val names = reader.lines()
    /* Read each line of the file and add collect the tags into a list */
    var tags = Set[Category]()
    names.forEach(name => tags += Category(name))
    /* Close the reader */
    reader.close()
    /* Return the tags */
    tags
  }

  /* The method to write the defined tags to their file */
  def storeTags(categories: Set[Category]): Unit = {
    /* Get the file where the tags are written */
    val tagsFile = Storage.categoriesFile.toFile
    val writer = tagsFile.bufferedWriter()
    /* Write the category names to the file */
    categories.foreach(category => {
      writer.append(category.name)
      writer.newLine()
    })
    /* Close the writer */
    writer.flush()
    writer.close()
  }

}
