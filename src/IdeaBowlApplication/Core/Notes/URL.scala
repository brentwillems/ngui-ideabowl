package IdeaBowlApplication.Core.Notes

/* The class representing an URL to a website */
case class URL(value: String)
