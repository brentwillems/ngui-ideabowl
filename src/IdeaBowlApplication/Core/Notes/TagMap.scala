package IdeaBowlApplication.Core.Notes

import java.io.Serializable

import IdeaBowlApplication.RFIDInterface.TagId

import scala.collection.mutable

/* The mapping of an RFID tag to a tag */
class TagMap(elems: (TagId, Category)*) extends mutable.HashMap[TagId, Category] with Serializable {
  /* Insert all provided elements into the map */
  ++=(elems)
}
