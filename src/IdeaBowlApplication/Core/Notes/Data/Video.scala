package IdeaBowlApplication.Core.Notes.Data

import scala.reflect.io.Path

/* The class representing a video file */
class Video(source: String) extends scalafx.scene.media.Media(source)


/* The companion object for videos */
object Video {

  /**************/
  /* Load Video */
  /**************/
  /* The method to load a video file from a given path */
  //def loadVideo(path: Path): Video = new Video(path.toURL.toExternalForm)
  def loadVideo(path: Path): Video = new Video(path.toURI.toString)

}
