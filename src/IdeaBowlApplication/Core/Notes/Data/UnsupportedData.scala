package IdeaBowlApplication.Core.Notes.Data

import scalafx.application.JFXApp

import scala.reflect.io.Path

/* The class representing data that the application can't display to the user */
class UnsupportedData(path: Path) extends Hyperlink(path.name) {

  /* The method to be called when the hyperlink is clicked */
  onAction = _ => {
    /* Open the file in the system's default application (not in this application) to display the data */
    JFXApp.ActiveApp.hostServices.delegate.showDocument(path.toURL.toExternalForm)
  }

}
