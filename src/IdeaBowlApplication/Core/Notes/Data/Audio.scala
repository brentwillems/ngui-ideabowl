package IdeaBowlApplication.Core.Notes.Data

import scala.reflect.io.Path

/* The class representing an audio file */
class Audio(source: String) extends scalafx.scene.media.Media(source)


/* The companion object for audio files */
object Audio {

  /**************/
  /* Load Audio */
  /**************/
  /* The method to load an audio file from a given path */
  //def loadAudio(path: Path): Audio = new Audio(path.toURL.toExternalForm)
  def loadAudio(path: Path): Audio = new Audio(path.toURI.toString)

}
