package IdeaBowlApplication.Core.Notes.Data

import scalafx.application.JFXApp

/* The class representing a hyperlink */
class Hyperlink(url: String) extends scalafx.scene.control.Hyperlink(url) {

  /* The method to be called when the hyperlink is clicked */
  onAction = _ => {
    /* Open the url in the system's default browser (outside the application) */
    JFXApp.ActiveApp.hostServices.delegate.showDocument(url)
  }

}
