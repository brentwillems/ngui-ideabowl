package IdeaBowlApplication.Core.Notes.Data

import java.io.FileInputStream

import scala.reflect.io.Path

/* The class representing an image */
class Image(inputStream: FileInputStream) extends scalafx.scene.image.Image(inputStream)


/* The companion object for images */
object Image {

  /**************/
  /* Load Image */
  /**************/
  /* The method to load an image from a given file path */
  def loadImage(path: Path): Image = {
    /* Create a file input stream for reading the image */
    val fileInputStream = new FileInputStream(path.jfile)
    /* Create and return a new image for the given file */
    new Image(fileInputStream)
  }

}
