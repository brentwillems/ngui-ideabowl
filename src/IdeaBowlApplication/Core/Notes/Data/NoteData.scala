package IdeaBowlApplication.Core.Notes.Data

import java.nio.file.Files

import IdeaBowlApplication.Core.Notes.Note
import IdeaBowlApplication.Core.Util.Storage

import scala.reflect.io.Path

/* The object used for loading and storing note data */
object NoteData {

  /************************/
  /* Data Types Supported */
  /************************/
  /* The data types supported by the scalafx (and javafx) application */
  /* Images => jpg, jpeg, bmp, png */
  /* Audio  => mp3, wav */
  /* Video  => mp4 */
  /* All other data formats are given to the user's system (OS) to open */


  /*************/
  /* Load Data */
  /*************/
  /* Method to load in the data for a given file path */
  def loadData(path: Path): Either[Either[Image, Either[Audio, Video]], UnsupportedData] = path.extension.toLowerCase match {
    /* Image */
    case "jpg" | "png" | "bmp" | "jpeg" => Left(Left(Image.loadImage(path)))
    /* Audio */
    case "mp3" | "wav" => Left(Right(Left(Audio.loadAudio(path))))
    /* Video */
    case "mp4" => Left(Right(Right(Video.loadVideo(path))))
    /* Default case for unsupported data */
    case _ => Right(new UnsupportedData(path))
  }

  /*******************/
  /* Load Hyperlinks */
  /*******************/
  /* Method to load in the hyperlinks for a given note */
  def loadHyperlinks(note: Note): List[Hyperlink] = {
    /* Get the urls from the note, and for each one, create a Hyperlink */
    note.urls.map(url => new Hyperlink(url.value))
  }

  /**************/
  /* Store Data */
  /**************/
  /* The method to save files at a given location to the data directory of a note */
  def storeData(note: Note, dataPaths: List[Path]): Unit = {
    /* Get the data directory of the note */
    val dataDirectory = Storage.getDataDirectory(note)
    /* Create the directory if necessary */
    if (!dataDirectory.exists) dataDirectory.createDirectory()
    /* For each data path */
    dataPaths.foreach(dataPath => {
      /* Get the name of the file */
      val fileName = dataPath.name
      /* Transform the source and destination paths */
      val source = java.nio.file.Paths.get(dataPath.toURI)
      val destination = java.nio.file.Paths.get(dataDirectory./(fileName).toURI)
      /* Copy the file at the source to the destination */
      Files.copy(source, destination)
    })
  }

}
