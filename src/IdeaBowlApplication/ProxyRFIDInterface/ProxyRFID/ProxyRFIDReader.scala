package IdeaBowlApplication.ProxyRFIDInterface.ProxyRFID

import IdeaBowlApplication.ProxyRFIDInterface.RFIDData
import IdeaBowlApplication.RFIDInterface.RFIDReader
import com.phidget22.{AttachEvent, DetachEvent, RFIDTagEvent, RFIDTagLostEvent}
import scalafx.application.Platform

/* The proxy class, mimicking the behaviour of the RFID hardware */
class ProxyRFIDReader extends RFIDReader {

  /**************************/
  /* Attach & Detach Events */
  /**************************/
  /* The event handler, when the attachment (connection) of an RFID reader is detected */
  def attachListener(attachEvent: AttachEvent): Unit = {
    /* Display the event in the console */
    try println(s"[${RFIDData.readerName}] Attached reader ${attachEvent.getSource.getPhidgetIDString}")
    catch {
      /* Phidget interface blocks core application exceptions => re-throw them */
      case exception: Exception => exception.printStackTrace()
    }
  }

  /* The event handler, when the detachment (disconnection) of an RFID reader is detected */
  def detachListener(detachEvent: DetachEvent): Unit = {
    /* Display the event */
    try println(s"[${RFIDData.readerName}] Detached reader ${detachEvent.getSource.getPhidgetIDString}")
    catch {
      /* Phidget interface blocks core application exceptions => re-throw them */
      case exception: Exception => exception.printStackTrace()
    }
  }

  /*********************/
  /* Handle Tag Events */
  /*********************/
  /* The TagEvent handler, when a tag is detected */
  def handleTagEvent(tagEvent: RFIDTagEvent): Unit = {
    try {
      /* Call the add tag method of the reader */
      Platform.runLater {
        /* Get the tagObject for the given tag ID */
        val tagObject = RFIDData.getTagObject(tagEvent.getTag)
        /* If the reader doesn't contain the tag, then the event was created by the proxy reader */
        if (!RFIDData.tagReader.containsTag(tagObject)) {
          /* Add the tag object to the reader */
          RFIDData.tagReader.addTagObject(tagObject)
        }
        /* Display the tag event */
        println(s"[${RFIDData.readerName}] Detected tag ${tagEvent.getTag}")
      }
    }
    catch {
      /* Phidget interface blocks core application exceptions => re-throw them */
      case exception: Exception => exception.printStackTrace()
    }
  }

  /* The TagEvent handler, when a tag is lost (not detected anymore) */
  def handleTagLostEvent(tagEvent: RFIDTagLostEvent): Unit = {
    try {
      /* Call the add tag method of the bowl */
      Platform.runLater {
        /* Get the tagObject for the given tag ID */
        val tagObject = RFIDData.getTagObject(tagEvent.getTag)
        /* If the reader doesn't contain the tag, then the event was created by the proxy reader */
        if (!RFIDData.tagBowl.containsTag(tagObject)) {
          /* Add the tag object to the bowl */
          RFIDData.tagBowl.addTagObject(tagObject)
        }
        /* Display the tag event */
        println(s"[${RFIDData.readerName}] Lost tag ${tagEvent.getTag}")
      }
    }
    catch {
      /* Phidget interface blocks core application exceptions => re-throw them */
      case exception: Exception => exception.printStackTrace()
    }
  }

  /******************/
  /* Initialisation */
  /******************/
  /* Add all the listeners to the underlying RFID listeners */
  addNewTagListener(handleTagEvent)
  addNewTagLostListener(handleTagLostEvent)
  addNewAttachListener(attachListener)
  addNewDetachListener(detachListener)

}
