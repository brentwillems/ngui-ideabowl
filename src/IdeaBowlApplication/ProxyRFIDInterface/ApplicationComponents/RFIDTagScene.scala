package IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents

import IdeaBowlApplication.ProxyRFIDInterface.RFIDData
import scalafx.beans.property.{DoubleProperty, ReadOnlyDoubleProperty}
import scalafx.geometry.{Insets, Orientation, Pos}
import scalafx.scene.Scene
import scalafx.scene.control.Separator
import scalafx.scene.layout.{HBox, VBox}
import scalafx.scene.text.Text

/* The class representing a scene, containing the proxy RFID interface */
class RFIDTagScene extends Scene {

  /*****************/
  /* Bowl & Reader */
  /*****************/
  /* Pane */
  val pane = new HBox
  /* The tag bowl title */
  val bowlTitle = new Text("Tag Bowl")
  /* The tag reader title */
  val readerTitle = new Text("Tag Reader")


  /******************/
  /* Initialisation */
  /******************/
  /* The method to initialise the scene */
  def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    /* Remove all children from the scene's pane */
    pane.children.clear()
    content.clear()
    content = pane
    /* Initialise the bowl and reader */
    RFIDData.tagBowl.initialise(preferenceWidth, preferenceHeight)
    val lastEventText = new Text
    lastEventText.text <== RFIDData.lastTagEventProperty

    /* The tag mapping */
    val tagMapping = new Text
    tagMapping.text <== RFIDData.tagMapping

    val readerHeight = DoubleProperty(0)
    readerHeight <== preferenceHeight / 3
    RFIDData.tagReader.initialise(preferenceWidth, readerHeight)

    /* Some UI layout */
    val bowlBox = new VBox
    bowlBox.children.addAll(bowlTitle, new Separator, RFIDData.tagBowl, new Separator)
    val separator = new Separator
    separator.orientation = Orientation.Vertical
    val readerBox = new VBox
    readerBox.children.addAll(readerTitle, new Separator, RFIDData.tagReader, new Separator, lastEventText,
      new Separator, tagMapping)

    pane.spacing = 20
    bowlBox.alignment = Pos.Center
    bowlBox.spacing = 5
    bowlBox.padding = Insets(5)
    RFIDData.tagBowl.vgap = 5
    RFIDData.tagBowl.hgap = 5
    RFIDData.tagBowl.alignment = Pos.TopCenter
    readerBox.alignment = Pos.TopCenter
    readerBox.spacing = 5
    readerBox.padding = Insets(5)
    RFIDData.tagReader.alignment = Pos.Center

    /* Add the bowl and reader to the scene */
    pane.children.addAll(bowlBox, separator, readerBox)
  }

}
