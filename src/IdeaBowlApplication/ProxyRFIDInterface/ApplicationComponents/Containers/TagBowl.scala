package IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Containers

import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Tag.TagObject
import IdeaBowlApplication.ProxyRFIDInterface.RFIDData
import IdeaBowlApplication.RFIDInterface.LostTagEvent
import scalafx.beans.property.ReadOnlyDoubleProperty

/* The class representing a tag bowl area */
class TagBowl extends TagContainer {

  /*****************/
  /* One Tag Limit */
  /*****************/
  /* The boolean, indicating whether or not the container only stores one tag, or it allows multiple */
  val oneTagOnly: Boolean = false


  /***************/
  /* Drag & Drop */
  /***************/
  /* The method to create a tag (lost) event for the given tagID */
  private def createTagEvent(tagID: String): LostTagEvent = new LostTagEvent(RFIDData.proxyRFIDReader, tagID)
  /* The proxy RFID reader handler to call f or the tag (lost) event */
  private def proxyReaderHandler(lostTagEvent: LostTagEvent): Unit = {
    RFIDData.proxyRFIDReader.tagLostListeners.foreach(listener => listener.onTagLost(lostTagEvent))
  }

  /* The method to add a tagObject with the given id to the bowl */
  override def addTagObject(tagObject: TagObject): Unit = {
    /* Only update the display if the tag isn't already in the container */
    if (tagObject.targetContainer.equals(this) && !containsTag(tagObject)) {
      /* Call the super method */
      super.addTagObject(tagObject)
      /* Get the tag id from the tag object */
      val tagID = tagObject.proxyTag.getTag
      /* Display the change */
      RFIDData.lastTagEventProperty.value = s"Reader: Lost tag $tagID"
      /* Remove the tag mapping for the detected tag */
      RFIDData.tagMapping.value = ""
    }
  }

  /* The method to add a tagObject with the given id to the reader and call the real handler */
  def addTagObjectAndHandle(tagObject: TagObject): Unit = {
    /* Add the tag object to the container */
    addTagObject(tagObject)
    /* Get the tag id from the tag object */
    val tagID = tagObject.proxyTag.getTag
    /* Create a tag event */
    val tagEvent = createTagEvent(tagID)
    /* Give it to the reader to handle */
    proxyReaderHandler(tagEvent)
  }

  /* When data is dropped */
  onDragDropped = dragDroppedHandler


  /******************/
  /* Initialisation */
  /******************/
  /* The method to initialise the tag bowl with all the tags in the application */
  override def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    /* Call the super method */
    super.initialise(preferenceWidth, preferenceHeight)
    prefHeight <== preferenceHeight * 0.8
    /* Add the tags to the container */
    RFIDData.tagObjectsDictionary.foreach(keyValue => {
      /* Get the tag object */
      val tagObject = keyValue._2
      /* Add the tag to the bowls list of children */
      insertTag(tagObject)
    })
  }

}
