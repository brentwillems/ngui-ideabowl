package IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Containers

import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Tag.TagObject
import IdeaBowlApplication.ProxyRFIDInterface.RFIDData
import javafx.scene.input.DragEvent
import scalafx.beans.property.ReadOnlyDoubleProperty
import scalafx.scene.input.TransferMode
import scalafx.scene.layout.FlowPane

/* The abstract class representing a tag container area */
abstract class TagContainer extends FlowPane {

  /*****************/
  /* One Tag Limit */
  /*****************/
  /* The boolean, indicating whether or not the container only stores one tag, or it allows multiple */
  val oneTagOnly: Boolean


  /************************/
  /* Insert & Remove Tags */
  /************************/
  /* The method to insert a tag into the container */
  def insertTag(tagObject: TagObject): Unit = children.add(tagObject)
  /* The method to remove a tag from the container */
  def removeTag(tagObject: TagObject): Unit = children.remove(tagObject)
  /* The method to check if the container has a tag as its child */
  def containsTag(tagObject: TagObject): Boolean = children.contains(tagObject)


  /***************/
  /* Drag & Drop */
  /***************/
  /* When data is dragged over */
  onDragOver = event => {
    /* Accept the Move move only if the drag isn't from this container */
    if (event.getGestureSource ne this) event.acceptTransferModes(TransferMode.Move)
    /* Consume the event */
    event.consume()
  }

  /* The method to add a tagObject with the given ID to the container */
  def addTagObject(tagObject: TagObject): Unit = {
    /* Avoid a ProxyLostTag event being fired when trying to move a tag from the bowl back in the bowl */
    if (tagObject.targetContainer.equals(this) && !containsTag(tagObject) && (!oneTagOnly || children.isEmpty)) {
      /* Add it to its children and remove it from the current container */
      tagObject.container.removeTag(tagObject)
      insertTag(tagObject)
      /* Update its container and target container */
      tagObject.targetContainer = tagObject.container
      tagObject.container = this
    }
  }

  /* The method to add a tagObject with the given id to the reader and call the real handler */
  def addTagObjectAndHandle(tagObject: TagObject): Unit


  /* The handler for drag dropped events */
  def dragDroppedHandler(event: DragEvent): Unit = {
    /* Check if the dragboard contains a string (tag ID) */
    val dragboard = event.getDragboard
    /* If the dragboard contains a string, add the corresponding tagObject to the container */
    if (dragboard.hasString) {
      /* Get the tag object with the given ID */
      val tagObject = RFIDData.getTagObject(dragboard.getString)
      /* Add the tag object to the container and call the handler */
      addTagObjectAndHandle(tagObject)
    }
    /* Consume the event */
    event.consume()
  }

  /* When data is dropped */
  onDragDropped = dragDroppedHandler


  /******************/
  /* Initialisation */
  /******************/
  /* The method to initialise the container */
  def initialise(preferenceWidth: ReadOnlyDoubleProperty, preferenceHeight: ReadOnlyDoubleProperty): Unit = {
    /* Remove all tags from the container */
    children.clear()
    /* Set the width and height */
    prefWidth <== preferenceWidth / 2 - 30
    prefHeight <== preferenceHeight
  }

}
