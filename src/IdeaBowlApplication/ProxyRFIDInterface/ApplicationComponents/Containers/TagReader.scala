package IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Containers

import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Tag.TagObject
import IdeaBowlApplication.ProxyRFIDInterface.RFIDData
import IdeaBowlApplication.RFIDInterface.{TagEvent, TagId}

/* The class representing a tag bowl area */
class TagReader extends TagContainer {

  /*****************/
  /* One Tag Limit */
  /*****************/
  /* The boolean, indicating whether or not the container only stores one tag, or it allows multiple */
  val oneTagOnly: Boolean = true


  /***************/
  /* Drag & Drop */
  /***************/
  /* The method to create a tag (lost) event for the given tagID */
  protected def createTagEvent(tagID: String): TagEvent = new TagEvent(RFIDData.proxyRFIDReader, tagID)
  /* The proxy RFID reader handler to call f or the tag (lost) event */
  protected def proxyReaderHandler(tagEvent: TagEvent): Unit = {
    RFIDData.proxyRFIDReader.tagListeners.foreach(listener => listener.onTag(tagEvent))
  }

  /* The method to add a tagObject with the given id to the reader */
  override def addTagObject(tagObject: TagObject): Unit = {
    /* Only update the display if the tag isn't already in the container */
    if (tagObject.targetContainer.equals(this) && !containsTag(tagObject) && (!oneTagOnly || children.isEmpty)) {
      /* Call the super method */
      super.addTagObject(tagObject)
      /* Get the tag id from the tag object */
      val tagID = tagObject.proxyTag.getTag
      /* Display the change */
      RFIDData.lastTagEventProperty.value = s"Reader: Detected tag $tagID"

      /* Get the category name from the mapping for the detected tag */
      try {
        val category = RFIDData.bowl.getTagMapping(TagId(tagID))
        RFIDData.tagMapping.value = s"$tagID -> ${category.name}"
      }
      catch {
        case _: NoSuchElementException =>
          /* Remove the tag mapping for the detected tag */
          RFIDData.tagMapping.value = s"$tagID -> _"
      }
    }
  }

  /* The method to add a tagObject with the given id to the reader and call the real handler */
  def addTagObjectAndHandle(tagObject: TagObject): Unit = {
    /* Add the tag object to the container */
    addTagObject(tagObject)
    /* Get the tag id from the tag object */
    val tagID = tagObject.proxyTag.getTag
    /* Create a tag event */
    val tagEvent = createTagEvent(tagID)
    /* Give it to the reader to handle */
    proxyReaderHandler(tagEvent)
  }

  /* When data is dropped */
  onDragDropped = dragDroppedHandler

}
