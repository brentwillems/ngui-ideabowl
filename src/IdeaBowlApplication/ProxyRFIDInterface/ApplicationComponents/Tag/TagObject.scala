package IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Tag

import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Containers.TagContainer
import IdeaBowlApplication.ProxyRFIDInterface.RFIDData.{tagBowl, tagReader}
import IdeaBowlApplication.RFIDInterface.TagEvent
import javafx.scene.image.Image
import scalafx.scene.SnapshotParameters
import scalafx.scene.control.{ContentDisplay, Label}
import scalafx.scene.image.WritableImage
import scalafx.scene.input.{ClipboardContent, TransferMode}
import scalafx.scene.paint.Color
import scalafx.scene.shape.Circle

/* The class representing a tag object */
class TagObject extends Label {

  /*************/
  /* Proxy Tag */
  /*************/
  /* The proxy tag of the tag object */
  var proxyTag: TagEvent = _
  var image: Image = _

  /* Colour the tag and set its style */
  private def setStyle(colour: Color): Unit = {
    /* Set the shape of the tag (circle) */
    val radius = 40
    val circle = Circle(radius = radius, fill = colour)
    circle.autosize()
    circle.stroke = Color.Black
    graphic = circle
    /* Display the tag ID in the center of the circle */
    contentDisplay = ContentDisplay.Center
    /* Avoid white text on white tag colour */
    if (colour.equals(Color.White) || colour.equals(Color.WhiteSmoke)) textFill = Color.Black
    else textFill = Color.White
    /* Store an image of the tag's circle, used when dragging the tag */
    val wImage: WritableImage = new WritableImage((radius + 1) * 2, (radius + 1) * 2)
    this.graphic.value.snapshot(new SnapshotParameters(), wImage)
    image = wImage
  }

  /***************/
  /* Drag & Drop */
  /***************/
  /* The container of the tag */
  var container: TagContainer = _
  /* The target container for the tag (container it can be moved to from the current) */
  var targetContainer: TagContainer = _

  /* When drag gesture is detected */
  onDragDetected = event => {
    /* Start the drag-and-drop gesture */
    val dragboard = startDragAndDrop(TransferMode.Move)
    /* Set the image of the dragview to the shape and colour of the tag */
    dragboard.setDragView(image)
    /* Set the data to be transferred */
    val content = new ClipboardContent
    content.putString(proxyTag.getTag)
    dragboard.setContent(content)
    /* Consume the event */
    event.consume()
  }

  /******************/
  /* Initialisation */
  /******************/
  /* The method to initialise the tag object to mimic the given tag event, and have the given tag colour */
  def initialise(proxy: TagEvent, colour: Color): Unit = {
    /* Set the proxy to mimic */
    proxyTag = proxy
    /* Set the current container and the future target */
    container = tagBowl
    targetContainer = tagReader
    /* Set the style of the tag object */
    setStyle(colour)
    text = "ID " + proxyTag.getTag
    prefWidth = 50
    prefHeight = 50
  }

}
