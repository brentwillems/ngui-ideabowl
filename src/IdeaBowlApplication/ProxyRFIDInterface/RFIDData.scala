package IdeaBowlApplication.ProxyRFIDInterface

import IdeaBowlApplication.Core.Bowl.Bowl
import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Containers.{TagBowl, TagReader}
import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.Tag.TagObject
import IdeaBowlApplication.ProxyRFIDInterface.ProxyRFID.ProxyRFIDReader
import IdeaBowlApplication.RFIDInterface.TagEvent
import scalafx.beans.property.StringProperty
import scalafx.scene.paint.Color

import scala.collection.mutable

/* The object containing the proxy RFID reader and proxy RFID tags in the application */
object RFIDData {

  /***************/
  /* RFID Reader */
  /***************/
  /* The proxy RFID reader of the application */
  var proxyRFIDReader: ProxyRFIDReader = _


  /********/
  /* Bowl */
  /********/
  /* The bowl, storing the notes and tags */
  val bowl = new Bowl


  /********************/
  /* UI Bowl & Reader */
  /********************/
  /* The tag bowl */
  val tagBowl = new TagBowl
  /* The tag reader */
  val tagReader = new TagReader

  /* The name of the RFID reader */
  val readerName = "RFIDReader"
  /* The property displaying the last event registered by the reader (real or proxy) */
  val lastTagEventProperty = StringProperty("")
  /* The list of tag detected, along with their corresponding mapping to note tags */
  var tagMapping = StringProperty("")


  /*************/
  /* RFID Tags */
  /*************/
  /* The collection of tag IDs */
  val allTagIDs: Array[String] = Array(
    /* Blue keychain tag */ "4a003796c5",
    /* Black tags */ "3000d77fee", "3000d72f62",
    /* White tags */ "5200544557", "5200541fd9", "5200541471",
    /* Transparent, sticker tags */ "520054116a", "5200541ec2", "4e002fa1c7", "5200540c63"
  )

  /* The collection of colours, corresponding to allTagIDs */
  val allColours: Array[Color] = Array(
    /* Blue tags */ Color.DarkBlue,
    /* Black tags */ Color.Black, Color.Black,
    /* White tags */ Color.White, Color.White, Color.White, Color.White,
    /* Transparent tags */ Color.WhiteSmoke, Color.WhiteSmoke, Color.WhiteSmoke, Color.WhiteSmoke
  )

  /**************************/
  /* Tag Objects Dictionary */
  /**************************/
  /* The dictionary of tag objects */
  val tagObjectsDictionary: mutable.Map[String, TagObject] = mutable.Map()

  /* Method to get a tag with the given ID */
  def getTagObject(tagID: String): TagObject =
    tagObjectsDictionary.getOrElse(tagID, throw new NoSuchElementException(s"No tagObject for the given ID: $tagID"))


  /******************/
  /* Initialisation */
  /******************/
  /* Method to initialise the RFID data */
  def initialise(newReader: ProxyRFIDReader): Unit = {
    /* Point to the new reader */
    proxyRFIDReader = newReader
    /* Create the tags and add them to the dictionary */
    for (index <- allTagIDs.indices) {
      /* For each tag ID and colour */
      val tagID = allTagIDs(index)
      val colour = allColours(index)
      /* Create a new tag */
      val proxyTag = new TagEvent(proxyRFIDReader, tagID)
      /* Create a tag object for the tag and initialise it */
      val tagObject = new TagObject
      tagObject.initialise(proxyTag, colour)
      /* Add the tag to the dictionary */
      tagObjectsDictionary.put(tagID, tagObject)
    }
  }

}
