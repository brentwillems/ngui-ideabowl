package IdeaBowlApplication.ProxyRFIDInterface

import IdeaBowlApplication.IdeaBowlInterface
import IdeaBowlApplication.ProxyRFIDInterface.ApplicationComponents.RFIDTagScene
import IdeaBowlApplication.ProxyRFIDInterface.ProxyRFID.ProxyRFIDReader
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.stage.Stage

/* The stage of the proxy application, for usage in the main application */
class ProxyRFIDStage extends Stage {

  /* The scene to display */
  val firstScene = new RFIDTagScene
  title = "Proxy RFID Application"
  scene = firstScene
  width = 800
  height = 500

  /* Initialise data */
  RFIDData.initialise(IdeaBowlInterface.reader)
  /* Initialise the scene, and its components */
  firstScene.initialise(width, height)

}

/* The object representing the standalone proxy application, allowing the mimicking of the RFID reader and tags functionality */
object ProxyRFIDApplication extends JFXApp {

  /* The scene to display */
  val firstScene = new RFIDTagScene

  /* The stage of the application */
  stage = new PrimaryStage {
    title = "Proxy RFID Application"
    scene = firstScene
    width = 800
    height = 500
  }

  /* Initialise data */
  RFIDData.initialise(new ProxyRFIDReader)
  /* Initialise the scene, and its components */
  firstScene.initialise(stage.width, stage.height)
  /* Open the RFID reader to allow it to listen for attaching/detaching readers and listen for tag events */
  RFIDData.proxyRFIDReader.open()

  /* When the application is closed, close the reader */
  stage.onCloseRequest = _ => RFIDData.proxyRFIDReader.close()

}
