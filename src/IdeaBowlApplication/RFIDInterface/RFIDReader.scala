package IdeaBowlApplication.RFIDInterface

/* The class representing an RFID reader */
import com.phidget22.{AttachListener, DetachListener, RFID, RFIDTagListener, RFIDTagLostListener}

/* The proxy class, mimicking the behaviour of the RFID hardware */
class RFIDReader extends RFID {

  /**************************/
  /* Attach & Detach Events */
  /**************************/
  /* The list of all attachment listeners (used by proxy reader) */
  var attachListeners: List[AttachListener] = List()
  /* The list of all detachment listeners (used by proxy reader) */
  var detachListeners: List[DetachListener] = List()

  /* Define new add listeners to also store the listeners in the lists */
  def addNewAttachListener(attachListener: AttachListener): Unit = {
    /* Call the super method */
    super.addAttachListener(attachListener)
    /* Store the listener in the corresponding list */
    attachListeners ::= attachListener
  }
  def addNewDetachListener(detachListener: DetachListener): Unit = {
    /* Call the super method */
    super.addDetachListener(detachListener)
    /* Store the listener in the corresponding list */
    detachListeners ::= detachListener
  }

  /*********************/
  /* Handle Tag Events */
  /*********************/
  /* The list of all attachment listeners (used by proxy reader) */
  var tagListeners: List[RFIDTagListener] = List()
  /* The list of all detachment listeners (used by proxy reader) */
  var tagLostListeners: List[RFIDTagLostListener] = List()

  /* Define new add listeners to also store the listeners in the lists */
  def addNewTagListener(tagListener: RFIDTagListener): Unit = {
    /* Call the super method */
    super.addTagListener(tagListener)
    /* Store the listener in the corresponding list */
    tagListeners ::= tagListener
  }
  def addNewTagLostListener(tagLostListener: RFIDTagLostListener): Unit = {
    /* Call the super method */
    super.addTagLostListener(tagLostListener)
    /* Store the listener in the corresponding list */
    tagLostListeners ::= tagLostListener
  }

}
