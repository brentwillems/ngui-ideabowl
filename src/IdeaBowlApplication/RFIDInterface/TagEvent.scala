package IdeaBowlApplication.RFIDInterface

import com.phidget22.{RFID, RFIDProtocol, RFIDTagEvent}

/* The class representing an RFID tag event: A tag that is detected */
class TagEvent(reader: RFID, tagID: String) extends RFIDTagEvent(reader, tagID, RFIDProtocol.EM4100)
