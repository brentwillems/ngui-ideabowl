package IdeaBowlApplication.RFIDInterface

/* The case class for a tag Id */
case class TagId(id: String)
