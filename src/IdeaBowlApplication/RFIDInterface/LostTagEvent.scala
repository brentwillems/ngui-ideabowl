package IdeaBowlApplication.RFIDInterface

import com.phidget22.{RFID, RFIDProtocol, RFIDTagLostEvent}

/* An RFID tag lost event: A tag that has lost detection */
class LostTagEvent(reader: RFID, tagID: String) extends RFIDTagLostEvent(reader, tagID, RFIDProtocol.EM4100)
