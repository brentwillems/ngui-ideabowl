package IdeaBowlApplication.Util

import IdeaBowlApplication.Core.Notes.{Category, URL}
import scala.collection.mutable.ListBuffer

object Parser {


  /* Textfield parsing */
  def parseTextField(textContent: String, splitCondition: String = "\n"): List[String] = {
    textContent.split(splitCondition).toList
  }

  /* Textfield parsing with custom mapping function
  *   mapping example: (element => Tag(element) for conversion of strings to Tags
  *   splitCondition: defaults to 'newline' to consider each line a separate entry */
  def parseCustomTextField[T](textContent: String, mappingFunc: String => T, splitCondition: String = "\n"): List[T] = {
    parseTextField(textContent, splitCondition).map(mappingFunc)
  }


  /* Parsing functions for specific data types */
  val tagParser: String => Category = tag => Category(tag)
  val urlParser: String => URL = url => URL(url)
  val dataParser: String => String = data => data




}
