package IdeaBowlApplication

import IdeaBowlApplication.Core.Bowl.Bowl
import IdeaBowlApplication.Core.Notes.Data.NoteData
import IdeaBowlApplication.Core.Notes.{Category, Note, URL}
import IdeaBowlApplication.ProxyRFIDInterface.ProxyRFID.ProxyRFIDReader
import IdeaBowlApplication.ProxyRFIDInterface.ProxyRFIDStage
import IdeaBowlApplication.UserInterface.HomeScreen

import scala.reflect.io.Path


/* This object contains all data, variables and instantiations of classes the main application needs to use */
object IdeaBowlInterface {

  /* Main components */
  var bowlInstance = new Bowl()

  /* The reader */
  val reader = new ProxyRFIDReader // TODO: replace by default reader when proxy no longer needed
  /* The proxy stage, for testing the application with the proxy reader */
  val proxyRFIDStage = new ProxyRFIDStage

  /* The home screen */
  val homeScreen = new HomeScreen


  /* Interface */
  def saveNote(idea: String, data: List[String], urls: List[String], tags: List[Category]): Unit = {
    /* Create a new note */
    val note = new Note
    note.idea = idea
    note.data = data.map(path => Path(path).name)
    NoteData.storeData(note, data.map(path => Path(path)))
    note.urls = urls.map(str => URL(str))
    note.categories = tags
    /* Add to Bowl */
    bowlInstance.insertNote(note)
  }


  /* Initialisation */
  def initialise(): Unit = {
    /* Initialise bowl */
    bowlInstance = Bowl.loadBowl()
    /* Initialise next id */
    Note.loadNextId()
  }

}
