package IdeaBowlApplication

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage

/* The object representing the Idea Bowl application */
object IdeaBowl extends JFXApp {

  /* Initialise Interface */
  IdeaBowlInterface.initialise()

  /* The stage of the application */
  stage = new PrimaryStage {
    title = "Idea Bowl"
    scene = IdeaBowlInterface.homeScreen
    width = 1280
    height = 720
  }
  /* Window Maximalisation / Fullscreen */
  stage.setMaximized(true)
  //stage.setFullScreen(true)

  /* Initialise the scene, and its components */
  IdeaBowlInterface.homeScreen.initialise(stage.width, stage.height)
  /* Show the proxy application */ //
  IdeaBowlInterface.proxyRFIDStage.show()

  /* Open the RFID reader to allow it to listen for attaching/detaching readers and listen for tag events */
  IdeaBowlInterface.reader.open()
  /* When the application is closed, close the reader */
  stage.onCloseRequest = _ => {
    /* Close the reader and the proxy */
    IdeaBowlInterface.reader.close()
    IdeaBowlInterface.proxyRFIDStage.close()
  }

}
